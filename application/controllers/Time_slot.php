<?php
class Time_slot extends CI_Controller{
public function __construct() {
        parent::__construct();
        if( !$this->session->userdata('doctor_id') ){
            return redirect('Doctorsignin');
    }
    }

public function index(){ 
 $doctor = $this->session->userdata('doctor_id');

 $doctor_id = $this->session->userdata('doctor_id');
 $this->load->model('editprofilemodel');
 $doctor_data = $this->editprofilemodel->get_id($doctor_id);
 $this->load->model('time_slotmodel');
 $text = $this->time_slotmodel->get_time($doctor);
 //print_r($doctor);die();
$this->load->view('public/time_slot',['text'=>$text,'doctor_data'=>$doctor_data]);
}
public function add_time(){
           
           $doctor = $this->session->userdata('doctor_id');
          
             $date = $this->input->post('date_from');
              $time  = $this->input->post('time_from');
              
              $this->load->model('time_slotmodel');
              $this->time_slotmodel->add_time($date,$time,$doctor);
              $data['text'] = $this->time_slotmodel->get_time($doctor);
              
              
               return redirect('Time_slot');

        
}
public function delt_time(){
	$doctor = $this->session->userdata('doctor_id');
	$date = $this->input->get('date_from');
	$time  = $this->input->get('time_from');
	$this->load->model('time_slotmodel');
	$this->time_slotmodel->remove_time($date,$time,$doctor);
	return redirect('Time_slot');
}
}