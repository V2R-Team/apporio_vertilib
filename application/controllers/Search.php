<?php
class Search extends CI_Controller
{
    public function lookup(){  
       $search_from = array();
        $keyword = $this->input->post('specialty');
        $search_from = $this->input->post('search_from');  
        $data['response'] = 'false'; //Set default response
        $this->load->model('autocomplete');
        $query = $this->autocomplete->lookup($keyword,$search_from); //Search DB  
        if( ! empty($query) )  
        {  
            $data['response'] = 'true'; //Set response  
            $data['message'] = array(); //Create array 
            switch( $search_from)
            { 
		case 'animal':            foreach( $query as $row )  
		            {  
		                $data['message'][] = array(   
		                                        'id'=>$row->id,  
		                                        'value' => $row->animal_name,  
		                                        '' 
		                                     ); 
		            } 
		            break;
		case 'specialty':            foreach( $query as $row )  
		            {  
		                $data['message'][] = array(   
		                                        'id'=>$row->specialty_id,  
		                                        'value' => $row->d_specialty,  
		                                        '' 
		                                     ); 
		            } 
		            break;
		case 'symptoms':            foreach( $query as $row )  
		            {  
		                $data['message'][] = array(   
		                                        'id'=>$row->id,  
		                                        'value' => $row->symptom_list,  
		                                        '' 
		                                     ); 
		            } 
		            break;   
		case 'clinic':       
		           foreach( $query as $row )  
		            {  
		                $data['message'][] = array(   
		                                        'id'=>$row->doctor_id,  
		                                        'value' => $row->d_clinic,  
		                                        '' 
		                                     ); 
		            } 
		            break;  
		default:
		foreach( $query as $row )  
		            {  
		                $data['message'][] = array(   
		                                        'id'=>$row->doctor_id,  
		                                        'value' => $row->d_firstname,  
		                                        '' 
		                                     ); 
		            } 
		            break;  
		                      
            } 
        }  
        if('IS_AJAX')  
        {  
            echo json_encode($data);  
        }  
        else 
        {  
            $this->load->view('public/search',$data);  
        }  
    }  
    public function look(){  
       
        $keyword = $this->input->post('city');  
        $data['response'] = 'false'; //Set default response
        $this->load->model('autocomplete');
        $query = $this->autocomplete->look($keyword); //Search DB  
        if( ! empty($query) )  
        {  
            $data['response'] = 'true'; //Set response  
            $data['message'] = array(); //Create array  
            foreach( $query as $row )  
            {  
                $city = $row->d_city.'-'.$row->zipcode;
                $data['message'][] = array(   
                                        'id'=>$row->city_id,  
                                        'value' =>$city,  
                                        '' 
                                     ); 
            }  
        }  
        if('IS_AJAX')  
        {  
            echo json_encode($data);  
        }  
        else 
        {  
            $this->load->view('public/search',$data); 
        }  
    }  
    public function doctor()          
    {    
          
          $specialty = $this->input->post('specialty');
          $search_from = $this->input->post('search_from');
          $city = $this->input->post('city'); 
          $date_slot = $this->input->post('date');
          $date_array = explode("/",$date_slot); // split the array
                       
      if(!empty($date_slot))
      {
$var_day = $date_array[0]; //day seqment
$var_month = $date_array[1]; //month segment
$var_year = $date_array[2]; //year segment
 
$new_date_format = "$var_day/$var_month/$var_year";
}
         
            if (!empty($specialty) && empty($city) && !empty($date_slot)){

               $this->load->model('searchmodel');
               $text = $this->searchmodel->time_slot($specialty,$new_date_format,$search_from);
              
              $this->load->view('public/search',['text'=>$text]);
              
                 }
          
             else if (!empty($city) && empty($specialty) && !empty($date_slot) ){
            
                    $this->load->model('searchmodel');
                     $text = $this->searchmodel->city_slot($city,$new_date_format);
                  
                     $this->load->view('public/search',['text'=>$text]); 
           
             }      
    
         else if (!empty($city) && !empty($specialty) && !empty($date_slot) ){ 
              
                    $this->load->model('searchmodel');
                     $text = $this->searchmodel->cs_slot($specialty,$city,$new_date_format);
                     
                    $this->load->view('public/search',['text'=>$text]);             
            
                       }
       else{
      
        return redirect('vertilib');  
       
       }
          
    }
}
