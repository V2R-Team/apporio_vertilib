<?php
class Bookingsignup extends CI_Controller{
    public function booking_signup(){
         
       $slot_id = $this->input->post('slot_id');
        $radio_val = $this->input->post('radio_val');
        $this->form_validation->set_rules('p_email', 'Email', 'trim|required|valid_email|matches[cp_email]|is_unique[patient.p_email]');
        $this->form_validation->set_rules('cp_email', 'Confirm Email', 'trim|required|valid_email'); 
        $this->form_validation->set_rules('p_password', 'Password', 'trim|required|min_length[10]');
        $this->form_validation->set_rules('p_firstname', 'FirstName', 'trim|required]');
        $this->form_validation->set_rules('p_lastname', 'Lastname', 'trim|required');
        $this->form_validation->set_rules('p_dob', 'Date of Birth', 'trim|required');
        $this->form_validation->set_rules('d_gender', 'Date of Birth', 'trim|required');
        if ($this->form_validation->run() == FALSE)
        {
              $this->load->model('searchmodel');
              $booking_data =$this->searchmodel->booking_slot($slot_id);
            $this->load->view('public/book_appointment', ['booking_data'=>$booking_data,'form1'=>$radio_val]);
        }
      else{
           
            
          
          $p_month = $this->input->post('p_dob');
                $p_day = $this->input->post('p_dob1');
                $p_year = $this->input->post('p_dob2');
                $p_dob = $p_year.'-'.$p_month.'-'.$p_day;
           $data = array(
                'p_email'     => $this->input->post('p_email'),
                'p_password'  => $this->input->post('p_password'),
                'p_firstname' => $this->input->post('p_firstname'),
                'p_lastname'  => $this->input->post('p_lastname'),
                'p_dob'       => $p_dob,
               'p_sex' => $this->input->post('d_gender')
            );
           
           $this->load->model('usermodel');
    $patient_id = $this->usermodel->user_signup($data);
           $this->load->model('searchmodel');
              $booking_data =$this->searchmodel->booking_slot($slot_id);
           if($patient_id){
              
              $this->session->set_userdata('patient_id',$patient_id);
                       
         $this->load->view('public/book_appointment_step2', ['booking_data'=>$booking_data,'patient_id'=>$patient_id]);
           }
           else{
               
          $this->load->view('public/book_appointment', ['booking_data'=>$booking_data,'form1'=>$radio_val]);
               }
      }
    
    }
}
