<?php
class Useraccount extends CI_Controller{
    public function __construct() {
        parent::__construct();
        if( !$this->session->userdata('patient_id') )
            return redirect('Signin');
    
    }
    public function index(){
   $data = $this->session->userdata('patient_id');
   $this->load->model('usermodel');
  $user_data = $this->usermodel->user_data($data);
  $this->load->view('public/useraccount',['user_data'=>$user_data]);
    }
    public function user_account(){
     $data = $this->session->userdata('patient_id');
       $this->load->model('usermodel');
       $user_data = $this->usermodel->user_data($data);
       $this->load->model('data');
       $app_data = $this->data->appointment_user($data);
       
        $this->load->view('public/past_appointment',['user_data'=>$user_data,'app_data'=>$app_data]);
        
    }
    public function settings(){
    $data = $this->session->userdata('patient_id');
    $this->load->model('usermodel');
     
    $user_data = $this->usermodel->user_data($data);
    $this->load->view('public/settings',['user_data'=>$user_data]);
    }
    public function change_password(){
    $data = $this->session->userdata('patient_id');
      $old_password = $this->input->post('oldpassword');
      $new_password = $this->input->post('p_password');
       $cp_password = $this->input->post('cp_password');
        $this->load->model('usermodel');
	  $user_data = $this->usermodel->user_data($data);
       if($new_password == $cp_password)
       {
	    $this->load->model('passwordmodel');
	    $user_data = $this->passwordmodel->update_password($data, $old_password,$new_password);
	    return redirect('Useraccount');
       }
       else
       {
       	    $error_pass="Password must be same for new and confirm password";
       	    $this->load->view('public/settings',['user_data'=>$user_data, 'tab_val'=>'password','error'=>$error_pass]);
       }
    }
     public function logout(){
        $this->session->unset_userdata('patient_id');
        $this->load->view('public/signin');
    }
}
