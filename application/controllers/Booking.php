<?php

class Booking extends CI_Controller{

    
    public function booking_time()
    {
       $slot_id = $this->input->get('slot_id');
       $this->load->model('searchmodel');
       $booking_data =$this->searchmodel->booking_slot($slot_id);
      $this->load->view('public/book_appointment', ['booking_data'=>$booking_data]);
    }
   
     public function booking_login()
    {
    if($this->input->get('book')){
   
    $patient_id = $this->session->userdata('patient_id');
     $slot_id = $this->input->get('slot_id');
      $this->load->model('searchmodel');
              $booking_data =$this->searchmodel->booking_slot($slot_id);
               $this->load->view('public/book_appointment_step2', ['booking_data'=>$booking_data,'patient_id'=>$patient_id]);
    }else{
          $email = $this->input->post('p_email');
           $radio_val = $this->input->post('radio_val');
          $password  = $this->input->post('p_password');
          $slot_id = $this->input->post('slot_id');
            $this->load->model('adminmodel');
            $patient_id = $this->adminmodel->login_user($email, $password);
             $this->load->model('searchmodel');
              $booking_data =$this->searchmodel->booking_slot($slot_id);
              
            if($patient_id){
             $this->session->set_userdata('patient_id',$patient_id);
               
            $this->load->view('public/book_appointment_step2', ['booking_data'=>$booking_data,'patient_id'=>$patient_id]);
    }
   else{
    		 $data["error"]="Invalid Email and Password combination";
    	     $this->load->view('public/book_appointment', ['booking_data'=>$booking_data,'form2'=>$radio_val,$data]);
       }}
    }
    public function book_2(){
        $slot_id = $this->input->post('slot_id');
         
        $payment = $this->input->post('payment');
        $reason = $this->input->post('reason');
        $new_old = $this->input->post('new_old');
        
        $this->load->model('adminmodel');
       $data = $this->adminmodel->slot_detail($slot_id);
 
        $patient_id = $this->input->post('patient_id');
         $this->load->model('searchmodel');
        $booking_data =$this->searchmodel->booking_slot($slot_id);
         $patient_data =$this->searchmodel->get_patient($patient_id);
       
        
        $this->adminmodel->booking_finish($data,$patient_id,$payment,$reason,$new_old,$slot_id);
       
      
       $email = $patient_data[0]['p_email'];

$msg = "<html>
    <head>
    <h1>Bonjour ". $patient_data[0]['p_firstname']."<h1>
    </head>
    <body>
        <p>Nous avons le plaisir de vous confirmer votre rendez vous du</p>
        <p>".$booking_data[0]['date_from']." à ". $booking_data[0]['time_from']."</p>
        <p>Les équipes de Veterilib la première plate-forme de prise de rdv vétérinaire en France</p>
         <p> vous souhaitent un bon rendez-vous.</p>
        <h4>Cordialement</h4>
        <h4>L’équipe Veterilib.</h4>
        <img src='http://apporio.org/vertilib/assets/images/Logo.jpg' alt='vertilib-logo' title='Vertilib' width='200'/>
    </body>
</html>" ;

$this->email->set_mailtype("html");
 $this->email->from('Veterilib.com');
$this->email->to($email); 

$this->email->subject('RDV vétérinaire confirmé!');
$this->email->message($msg);	

$this->email->send();
$email1 = $booking_data[0]['d_email'];
$msg1 = "<html>
    <head>
    <h1>Bonjour ". $booking_data[0]['d_firstname']."<h1>
    </head>
    <body>
        <p>Nous avons le plaisir de vous confirmer un nouveau rendez vous le </p>
        <p>".$booking_data[0]['date_from']." à ". $booking_data[0]['time_from']."</p>
        <p>Les équipes de Veterilib la première plate-forme de prise de rdv vétérinaire en France</p>
         <p> vous souhaitent un bon rendez-vous.</p>
        <h4>Cordialement</h4>
        <h4>L’équipe Veterilib.</h4>
        <img src='http://apporio.org/vertilib/assets/images/Logo.jpg' alt='vertilib-logo' title='Vertilib' width='200'/>
    </body>
</html>" ;

$this->email->set_mailtype("html");
 $this->email->from('Veterilib.com');
$this->email->to($email1); 

$this->email->subject('Un nouveau RDV');
$this->email->message($msg1);	

$this->email->send();
        $this->load->view('public/book_appointment_step4',['booking_data'=>$booking_data]);
        
    }
}

