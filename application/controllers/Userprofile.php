<?php
class Userprofile extends CI_Controller{
public function __construct() {
        parent::__construct();
        if( !$this->session->userdata('patient_id') )
            return redirect('Signin');
    
    }

    public function update(){
   
        $data = array(
                'p_email'     => $this->input->post('p_email'), 
                'p_firstname' => $this->input->post('p_firstname'),
                'p_lastname'  => $this->input->post('p_lastname'),
                'p_dob'       => $this->input->post('p_dob'),
                'p_city'       => $this->input->post('p_city'),
                 'p_phone'       => $this->input->post('p_phone'),
                 'a_address'       =>$this->input->post('a_address'),
                 's_address'       =>$this->input->post('s_address'),
                 'p_sex' => $this->input->post('p_sex')
            );
          
            $this->load->model('editprofilemodel');
            $user_data[0] = $this->editprofilemodel->edit_user($data);
           
           $msg="Profile is updated Successfully!";
            $this->load->view('public/settings',['user_data'=>$user_data, 'tab_val'=>'profile','success'=>$msg]);
    }
    }