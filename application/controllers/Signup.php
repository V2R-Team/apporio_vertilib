<?php
class Signup extends CI_Controller{
    public function index(){
        $this->load->view('public/signup');
    }
    public function new_user(){
         
       
        $this->form_validation->set_rules('p_email', 'Email', 'trim|required|valid_email|matches[cp_email]|is_unique[patient.p_email]');
        $this->form_validation->set_rules('cp_email', 'Confirm Email', 'trim|required|valid_email'); 
        $this->form_validation->set_rules('p_password', 'Password', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('p_firstname', 'FirstName', 'trim|required]');
        $this->form_validation->set_rules('p_lastname', 'Lastname', 'trim|required');
        $this->form_validation->set_rules('p_dob', 'Date of Birth', 'trim|required');
        $this->form_validation->set_rules('radio', 'Date of Birth', 'trim|required');
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('public/signup');
        }
      else{
          $p_month = $this->input->post('p_dob');
                $p_day = $this->input->post('p_dob1');
                $p_year = $this->input->post('p_dob2');
                $p_dob = $p_year.'-'.$p_month.'-'.$p_day;
           $data = array(
                'p_email'     => $this->input->post('p_email'),
                'p_password'  => $this->input->post('p_password'),
                'p_firstname' => $this->input->post('p_firstname'),
                'p_lastname'  => $this->input->post('p_lastname'),
                'p_dob'       => $p_dob,
               'p_sex' => $this->input->post('radio')
            );
           
           $this->load->model('usermodel');
       $data = $this->usermodel->user_signup($data);
       
              
           $this->session->set_userdata('patient_id', $data) ;
            return redirect('Useraccount');
      }
    
    }
}
