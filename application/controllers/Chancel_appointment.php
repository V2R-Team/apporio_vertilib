<?php
class Chancel_appointment extends CI_Controller{
    public function __construct() {
        parent::__construct();
       $this->load->model('chancel_appointmentmodel');
    }
    public function delt_appoint(){
        
        $user = $this->session->userdata('patient_id');
        $app = $this->input->get('app_id');
        $slot = $this->input->get('slot_id');
        
        $this->chancel_appointmentmodel->remove_appoint($user,$app,$slot);
        return redirect('Useraccount/user_account');
    }
    public function chancel_appoint(){
        
        $user = $this->session->userdata('doctor_id');
        $app = $this->input->get('app_id');
        $slot = $this->input->get('slot_id');
        
        $this->chancel_appointmentmodel->chancel_appoint($user,$app,$slot);
        return redirect('Account/appoint');
    }
}