<?php
class Doctor extends CI_Controller{
    public function index(){
        $this->load->model('data');

       $city = $this->data->city();


        $speciality = $this->data->specialty();
        
      $animal = $this->data->animal();
        $this->load->view('public/list_vertilib',['city'=>$city,'speciality'=>$speciality,'animal'=>$animal]);
    }
    public function signup(){
                $config = [
                            'upload_path' => './uploads/',
                            'allowed_types' => 'gif|jpg|png|jpeg'
                            
                ];
                $this->load->library('upload', $config);
                
            
        $this->form_validation->set_rules('d_firstname', 'First Name', 'trim|required|alpha');
        $this->form_validation->set_rules('d_lastname', 'Last Name', 'trim|required|alpha');
        $this->form_validation->set_rules('d_phone', 'Phone Number', 'trim|required'); 
        $this->form_validation->set_rules('specialty', 'Your Specialty', 'trim|required');
        $this->form_validation->set_rules('city', 'Your city', 'trim|required');
        $this->form_validation->set_rules('d_password', 'Password', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('d_email', 'Email', 'trim|required|valid_email|is_unique[doctor.d_email]');  
        $this->form_validation->set_rules('d_clinic', 'Clinic Address', 'trim|required');
        if ($this->form_validation->run() == FALSE ) {
       
               $this->load->model('data');

            $city = $this->data->city();


          $speciality = $this->data->specialty();
          $error =  $this->upload->display_errors();
           $animal = $this->data->animal();
        $this->load->view('public/list_vertilib',['city'=>$city,'speciality'=>$speciality,'error'=>$error,'animal'=>$animal]);
             
          } 
          else{
          if($this->upload->do_upload('d_image') == FALSE){
          
               $this->load->model('data');

            $city = $this->data->city();


          $speciality = $this->data->specialty();
          $error =  $this->upload->display_errors();
           $animal = $this->data->animal();
        $this->load->view('public/list_vertilib',['city'=>$city,'speciality'=>$speciality,'error'=>$error,'animal'=>$animal]);
          }
          else{
          
           $data = $this->upload->data(); 
             $image = base_url("uploads/".$data['raw_name'].$data['file_ext']);
               
            $animal = json_encode($this->input->post('animal')); 
            
          $data = array(
               
                'd_firstname' => $this->input->post('d_firstname'),
                'd_lastname' => $this->input->post('d_lastname'),
                'specialty_id' => $this->input->post('specialty'),
                'd_email' => $this->input->post('d_email'),
                'd_password' => $this->input->post('d_password'),
                'city_id' => $this->input->post('city'),
                'd_phone' => $this->input->post('d_phone'),
               'd_image'=> $image,
                'animal' => $animal,
                'd_clinic' => $this->input->post('d_clinic')
                
                );
                $this->load->model('adminmodel');
                $doc_id = $this->adminmodel->user_signup($data);
               
                 $this->session->set_userdata('doctor_id', $doc_id) ;
                
               return redirect('Account');
               }
          }
    }
} 