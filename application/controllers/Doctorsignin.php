<?php
class Doctorsignin extends CI_Controller{
public function index(){
    $this->load->view('public/dsignin');
}
 public function login_user()
    { 
       
    
        $this->form_validation->set_rules("d_email", "Email", "trim|required");
        $this->form_validation->set_rules("d_password", "Password", "trim|required");
       
          if ($this->form_validation->run() == FALSE) {
              
              $this->load->view('public/dsignin');
          }
          else{
              $email = $this->input->post('d_email');
              $password  = $this->input->post('d_password');
              $this->load->model('usermodel');
              $data = $this->usermodel->doc_user($email,$password);
              if ($data >0) {


               $this->session->set_userdata('doctor_id', $data) ;
                    
                   return redirect('Account');
              }
              else{    
              $data["error"]="Invalid Email and Password combination";
                 $this->load->view('public/dsignin',$data);
                
              }
              }
}
}
