<?php
class Account extends CI_Controller{

       public function __construct() {
        parent::__construct();
        if( !$this->session->userdata('doctor_id') )
            return redirect('Doctorsignin');
    }
   

    public function index(){
    
           
    $doctor_id = $this->session->userdata('doctor_id'); 
    $this->load->model('time_slotmodel');
  $text = $this->time_slotmodel->get_time($doctor_id);
    $this->load->model('editprofilemodel');
     $doctor_data = $this->editprofilemodel->get_id($doctor_id);

      $this->load->view('public/dashboard',['text'=>$text,'doctor_data'=>$doctor_data]);
      
    }
   
    public function appoint(){
     
       $doctor_id = $this->session->userdata('doctor_id');
      
      $this->load->model('data');
      $text = $this->data->appointment($doctor_id);
      
     
      $this->load->model('editprofilemodel');
     $doctor_data = $this->editprofilemodel->get_id($doctor_id);
      $this->load->view('public/appointmentdetail',['text'=>$text,'doctor_data'=>$doctor_data]);  
    }
    public function edit_profile(){
    $doctor_id = $this->session->userdata('doctor_id');
     
      $this->load->model('editprofilemodel');
      $doctor_data = $this->editprofilemodel->get_id($doctor_id);
       
     $this->load->model('data');
    $city = $this->data->city();
      $this->load->view('public/editprofile',['doctor_data'=>$doctor_data,'city'=>$city]);  
    }
    
    public function edit_profile_data()
    {
        $this->form_validation->set_rules('d_email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('d_phone', 'Mobile', 'trim|required'); 
        $this->form_validation->set_rules('dob', 'Date Of Birth', 'trim');
       
        $this->form_validation->set_rules('d_gender', 'Gender', 'required');
        if ($this->form_validation->run() == FALSE)
        {
         $doctor_id = $this->session->userdata('doctor_id');
     
      $this->load->model('editprofilemodel');
      $doctor_data = $this->editprofilemodel->get_id($doctor_id);
    
      $this->load->view('public/editprofile',['doctor_data'=>$doctor_data]);  
        }
      else
      {
   
                $data = array(
                'd_email' => $this->input->post('d_email'),
      		'd_phone' => $this->input->post('d_phone'),
      		
      		
                'd_gender' => $this->input->post('d_gender'),
                'd_dob'       =>$this->input->post('d_dob'),
                'city_id' => $this->input->post('d_city'),
                'da_address' => $this->input->post('da_address'),
               'ds_address' => $this->input->post('ds_address')
            );
           
           $this->load->model('editprofilemodel');  
           $this->editprofilemodel->edit_profile($data);
           $doctor_id = $this->session->userdata('doctor_id');
      $this->load->model('data');
     $city = $this->data->city();
           $this->load->model('editprofilemodel');
      $doctor_data = $this->editprofilemodel->get_id($doctor_id);
   $msg="Profile is updated Successfully!";
      $this->load->view('public/editprofile',['doctor_data'=>$doctor_data,'city'=>$city,'success'=>$msg]);  
      }
    }
     public function password(){
     $doctor_id = $this->session->userdata('doctor_id');
     $this->load->model('data');

       $city = $this->data->city();
    $this->load->model('editprofilemodel');
     $doctor_data = $this->editprofilemodel->get_id($doctor_id);

      $this->load->view('public/change-password',['city'=>$city,'doctor_data'=>$doctor_data]);
        
    }
   
     public function change_password(){
         $doctor_id = $this->session->userdata('doctor_id');
         $old_password = $this->input->post('oldpassword');
          $new_password = $this->input->post('d_password');
          $cd_password = $this->input->post('cd_password');
     
     if ($new_password == $cd_password)
        {
        $error_pass="Password is Changed";
          $this->load->model('passwordmodel');
	  $this->passwordmodel->change_password($doctor_id, $old_password,$new_password);
	   $this->load->model('data');
         $city = $this->data->city();
        $this->load->model('editprofilemodel');
        $doctor_data = $this->editprofilemodel->get_id($doctor_id);
         $this->load->view('public/change-password',['city'=>$city,'doctor_data'=>$doctor_data,'error'=>$error_pass]);
     
        }
        else{
        $error_pass="Password must be same for new and confirm password";
        $this->load->model('data');
      $city = $this->data->city();
    $this->load->model('editprofilemodel');
     $doctor_data = $this->editprofilemodel->get_id($doctor_id);
      $this->load->view('public/change-password',['city'=>$city,'doctor_data'=>$doctor_data,'error'=>$error_pass]);
        } 
        }
       public function logout(){
        $this->session->unset_userdata('doctor_id');
        $this->load->view('public/dsignin');
    }
    
}

