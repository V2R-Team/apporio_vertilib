<?php
class Vertilib extends CI_Controller{
    public function index(){
       $data = $this->session->userdata('patient_id');
        $this->load->model('usermodel');
        $user_data = $this->usermodel->user_data($data);
        $this->load->view('public/index',['user_data'=>$user_data]);
    }
   
}

