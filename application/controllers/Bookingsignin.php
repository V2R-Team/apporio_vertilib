<?php

class Bookingsignin extends CI_Controller{
    public function signin(){
        $this->form_validation->set_rules('p_email', 'Email', 'trim|required');

        $this->form_validation->set_rules('p_password', 'Password', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
           
             return redirect('BookingSignin');
        }
else{
                $email = $this->input->post('p_email');
                $password = $this->input->post('p_password');
                
                $this->load->model('usermodel');
                $this->usermodel->login_user($email,$password);
                
                 return redirect('Booking');
}
    }
}

