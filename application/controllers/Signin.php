<?php
class Signin extends CI_Controller{
    public function index(){
        $this->load->view('public/signin');
    }
    public function login_user()
    { 
       
    
        $this->form_validation->set_rules("p_email", "Email", "trim|required");
        $this->form_validation->set_rules("p_password", "Password", "trim|required");
       
          if ($this->form_validation->run() == FALSE) {
              
              $this->load->view('public/signin');
          }
          else{
              $email = $this->input->post('p_email');
              $password  = $this->input->post('p_password');
              $this->load->model('adminmodel');
              $data = $this->adminmodel->login_user($email,$password);
              if ($data >0) {


               $this->session->set_userdata('patient_id', $data) ;
                    
                    return redirect('Useraccount');
              }
              else{    
              $data["error"]="Invalid Email and Password combination";
                 $this->load->view('public/signin',$data);
                
              }
              
          }
          
    }
}