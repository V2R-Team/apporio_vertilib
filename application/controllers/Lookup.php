<?php
class Lookup extends CI_Controller{
public function index(){
          $specialty = $this->input->post('d_firstname');
          
          $city = $this->input->post('city'); 
          if (empty($specialty) && !empty($city)){

               $this->load->model('searchmodel');
               $text = $this->searchmodel->doctor_slot($city);
              
              
              $this->load->view('public/data',['text'=>$text]);
              
                 }
            else if (!empty($specialty) && empty($city)){
               $this->load->model('searchmodel');
               $text = $this->searchmodel->doctor_name($specialty);
              
              
              $this->load->view('public/data',['text'=>$text]);
              
                 } 
           else if (!empty($specialty) && !empty($city)){
           
               $this->load->model('searchmodel');
               $text = $this->searchmodel->doctor_data($specialty,$city);
              
              
              $this->load->view('public/data',['text'=>$text]);
              
                 }
                 else{
                 return redirect('vertilib');
                 }     
}
public function citydata(){  
       
        $city= $this->input->post('city');
        $data['response'] = 'false'; //Set default response
        $this->load->model('usermodel');
       
        $query = $this->usermodel->get_autocomplete($city); //Search DB  
        if( ! empty($query) )  
        {  
            $data['response'] = 'true'; //Set response  
            $data['message'] = array(); //Create array  
            foreach( $query as $row )  
            {  
                $city = $row->d_city.'-'.$row->zipcode;
                $data['message'][] = array(   
                                        'id'=>$row->city_id,  
                                        'value' =>$city,  
                                        '' 
                                     ); 
            }  
        }  
        if('IS_AJAX')  
        {  
            echo json_encode($data);  
        }  
        else 
        {  
            $this->load->view('public/header',$data);  
        }  
    }  
    public function doctor_data(){  
       
        $key= $this->input->post('d_firstname');
        $data['response'] = 'false'; //Set default response
        $this->load->model('usermodel');
       
        $query = $this->usermodel->get_clinic($key); //Search DB  
        if( ! empty($query) )  
        {  
            $data['response'] = 'true'; //Set response  
            $data['message'] = array(); //Create array  
            foreach( $query as $row )  
            {  
                $city = $row->d_firstname;
                $data['message'][] = array(   
                                        'id'=>$row->doctor_id,  
                                        'value' =>$city,  
                                        '' 
                                     ); 
            }  
        }  
        if('IS_AJAX')  
        {  
            echo json_encode($data);  
        }  
        else 
        {  
            $this->load->view('public/header',$data);  
        }  
    }  

}