<!DOCTYPE html >
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= link_tag('assets/css/style.css') ?>
    <title>Admin Panel</title>
</head>

<body>

<div class="container">
    <h2>Create Time Slot</h2>
    <?php echo form_open('admin/signup',['class'=>'form-horizontal']) ?>
    <div class="form-group">
        <label class="control-label col-sm-1">Time Slot:</label>
        <div class="col-sm-11">
            <?php echo form_input(['name'=>'time_from','class'=>'form-control','value'=>set_value('time_from')])?>
            <?php echo form_error('username');?>
        </div>
    </div>

    <div class="form-group" >
        <div class="col-sm-1"></div>
        <div class="col-sm-5">
            <?php echo form_submit(['name'=>'submit','class'=>'btn btn-default col-sm-6','id'=>'submit','value'=>'Create'])?>
        </div>

        <?php echo form_close(); ?>
    </div>
</body>
</html>