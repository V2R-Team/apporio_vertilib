<!DOCTYPE html >
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= link_tag('assets/css/style.css') ?>
    <title>Admin Panel</title>
</head>

<body>

<div class="container">
    <h2>Login In</h2>
    <?php echo form_open('admin/login',['name'=>'login','class'=>'form-horizontal']) ?>
    <div class="form-group">
        <label class="control-label col-sm-1" style="text-align: left;" for="email">Email:</label>
        <div class="col-sm-11">
            <?php echo form_input(['name'=>'username','class'=>'form-control','id'=>'email','placeholder'=>'Enter email','value'=>set_value('p_email')])?>
            <?php echo form_error('username');?>
        </div>

    </div>
    <div class="form-group">
        <label class="control-label col-sm-1" style="text-align: left;">Password:</label>
        <div class="col-sm-11">
            <?php echo form_password(['name'=>'d_password','class'=>'form-control','id'=>'password','placeholder'=>'Enter Password'])?>
            <?php echo form_error('d_password',"<p class='text-denger'>","</p>");?>
        </div>

    </div>

    <div class="form-group" >
        <div class="col-sm-1"></div>
        <div class="col-sm-5">
            <?php echo form_submit(['name'=>'submit','class'=>'btn btn-default col-sm-6','id'=>'submit','value'=>'login'])?>
        </div>
        <div class="col-sm-5">

            <?= anchor('admin/new_user','New user',['class'=>'btn btn-default col-sm-6'])?>

        </div>
    </div>
    <div class="form-group">

    </div>
    <?php echo form_close(); ?>
</div>
</body>
</html>