<!DOCTYPE html >
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= link_tag('assets/css/style.css') ?>
    <title>Admin Panel</title>
</head>

<body>

<div class="container">
    <h2>New doctor Register</h2>
    <?php echo form_open('admin/signup',['name'=>'Signup','class'=>'form-horizontal']) ?>
    <div class="form-group">
        <label class="control-label col-sm-1">Username:</label>
        <div class="col-sm-11">
            <?php echo form_input(['name'=>'username','class'=>'form-control','placeholder'=>'Enter username','value'=>set_value('username')])?>
            <?php echo form_error('username');?>
        </div>

    </div>
    <div class="form-group">
        <label class="control-label col-sm-1">Password:</label>
        <div class="col-sm-11">
            <?php echo form_password(['name'=>'d_password','class'=>'form-control','id'=>'password','placeholder'=>'Enter Password'])?>
            <?php echo form_error('d_password',"<p class='text-denger'>","</p>");?>
        </div>

    </div>

    <div class="form-group">
        <label class="control-label col-sm-1">FirstName:</label>
        <div class="col-sm-11">
            <?php echo form_input(['name'=>'d_firstname','class'=>'form-control','placeholder'=>'Enter First Name','value'=>set_value('d_firstname')])?>
            <?php echo form_error('d_firstname');?>
        </div>

    </div>

    <div class="form-group">
        <label class="control-label col-sm-1">LastName:</label>
        <div class="col-sm-11">
            <?php echo form_input(['name'=>'d_lastname','class'=>'form-control','placeholder'=>'Enter Last Name','value'=>set_value('d_lastname')])?>
            <?php echo form_error('d_lastname');?>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-1">Specialty:</label>
        <div class="col-sm-11">
            <select class="form-control" name="d_specialty">
                <option value="none" selected="selected">--Select your Specialty--</option>
                <?php if(count($text1)):?>
                    <?php foreach ($text1 as $text1):?>

                        <option><?= $text1->d_specialty ?></option>
                    <?php endforeach;?>
                <?php endif;?>
            </select>
            <?php echo form_error('d_specialty');?>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-1">Email:</label>
        <div class="col-sm-11">
            <?php echo form_input(['name'=>'d_email','class'=>'form-control','placeholder'=>'Enter Email','value'=>set_value('d_email')])?>
            <?php echo form_error('d_email');?>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-1">Mobile:</label>
        <div class="col-sm-11">
            <?php echo form_input(['name'=>'d_phone','class'=>'form-control','placeholder'=>'Enter Mobile Number','value'=>set_value('d_phone')])?>
            <?php echo form_error('d_phone');?>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-1">city:</label>
        <div class="col-sm-11">
            <select class="form-control" name="d_city">
                <option value="none" selected="selected">--Select your city--</option>
                <?php if(count($text)):?>
                <?php foreach ($text as $text):?>

                <option><?= $text->d_city ?></option>
                <?php endforeach;?>
                <?php endif;?>
            </select>
            <?php echo form_error('d_city');?>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-1">Gender:</label>
        <div class="col-sm-11">
            <input type="radio" name="d_gender" value="male" > Male
            <input type="radio" name="d_gender" value="female"> Female
            <input type="radio" name="d_gender" value="other"> Other
            <?php echo form_error('d_gender');?>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-1">city:</label>
        <div class="col-sm-11">
            <?php echo form_upload(['name'=>'d_image','class'=>'form-control','value'=>set_value('d_image')])?>
            <?php echo form_error('d_image');?>
        </div>
    </div>

    <div class="form-group" >
        <div class="col-sm-1"></div>
        <div class="col-sm-5">
            <?php echo form_submit(['name'=>'submit','class'=>'btn btn-default col-sm-6','id'=>'submit','value'=>'login'])?>
        </div>

    <?php echo form_close(); ?>
</div>
</body>
</html>