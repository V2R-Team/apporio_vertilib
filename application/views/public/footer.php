<!-- *************** Footer Start *************** -->
<div class="footer_one_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="footer_one_text">
                    Need help booking online? &nbsp; (123) 456-7890
                </div>
            </div>
            <div class="col-md-5">
                <div class="footer_one_text2">
                    <a href="#">Service@Veterilib.com</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="footer_list">
                    <span>Veterilib</span>

                    <ul class="footer_menu">
                        <li> <a href="#" title="About">About</a> </li>
                        <li> <a href="#" title="Press">Press</a> </li>
                        <li> <a href="#" title="Careers">Careers</a> </li>
                        <li> <a href="#" title="Contact">Contact</a> </li>
                        <li> <a href="#" title="Answers">Answers</a> </li>
                        <li> <a href="#" title="FAQ">FAQ</a> </li>
                        <li> <a href="#" title="Blog">Blog</a> </li>
                        <li> <a href="#" title="Doctor Blog">Doctor Blog</a> </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-2">
                <div class="footer_list">
                    <span>Search By</span>

                    <ul class="footer_menu">
                        <li> <a href="#" title="Doctor Name">Doctor Name</a> </li>
                        <li> <a href="#" title="Practice Name">Practice Name</a> </li>
                        <li> <a href="#" title="Specialty">Specialty</a> </li>
                        <li> <a href="#" title="Procedure">Procedure</a> </li>
                        <li> <a href="#" title="Language">Language</a> </li>
                        <li> <a href="#" title="Location">Location</a> </li>
                        <li> <a href="#" title="Hospital">Hospital</a> </li>
                        <li> <a href="#" title="Insurance">Insurance</a> </li>
                    </ul>
                </div>
            </div>


            <div class="col-md-2">
                <div class="footer_list">
                    <span>Cities</span>

                    <ul class="footer_menu">
                        <li> <a href="#" title="Chicago">Chicago</a> </li>
                        <li> <a href="#" title="Houston">Houston</a> </li>
                        <li> <a href="#" title="New York">New York</a> </li>
                        <li> <a href="#" title="Philadelphia">Philadelphia</a> </li>
                        <li> <a href="#" title="Phoenix">Phoenix</a> </li>
                        <li> <a href="#" title="San Antonio">San Antonio</a> </li>
                        <li> <a href="#" title="Washington DC">Washington DC</a> </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-2">
                <div class="footer_list">
                    <span>Specialties</span>

                    <ul class="footer_menu">
                        <li> <a href="#" title="Chiropractors">Chiropractors</a> </li>
                        <li> <a href="#" title="Dentists">Dentists</a> </li>
                        <li> <a href="#" title="Dermatologists">Dermatologists</a> </li>
                        <li> <a href="#" title="Eye Doctors">Eye Doctors</a> </li>
                        <li> <a href="#" title="Gynecologists">Gynecologists</a> </li>
                        <li> <a href="#" title="Primary Care Doctors">Primary Care Doctors</a> </li>
                        <li> <a href="#" title="Psychiatrists">Psychiatrists</a> </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-2">
                <div class="footer_list">
                    <span>Are You a Top Doctor?</span>

                    <ul class="footer_menu">
                        <li> <a href="#" title="Join Veterilib today!">Join Veterilib today!</a> </li>
                    </ul>
                </div>

                <br>

                <div class="footer_list">
                    <span>Veterilib for Employers</span>

                    <ul class="footer_menu">
                        <li> <a href="#" title="Learn More">Learn More</a> </li>
                    </ul>
                </div>

                <br>

                <div class="footer_list">
                    <span>Veterilib for Health Systems</span>

                    <ul class="footer_menu">
                        <li> <a href="#" title="Learn More">Learn More</a> </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-2">
                <div class="footer_list">
                    <span>Follow Veterilib</span>

                    <ul class="footer_menu">
                        <li> <a href="#" title="Facebook"> <i class="fa fa-facebook" aria-hidden="true"></i>  Facebook</a> </li>
                        <li> <a href="#" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter </a> </li>
                        <li> <a href="#" title="Google+"><i class="fa fa-google-plus" aria-hidden="true"></i> Google+ </a> </li>
                        <li> <a href="#" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i> Linkedin </a> </li>
                    </ul>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="footer_copy_text">
                    Our <a href="#" title="Privacy Policy</">Privacy Policy</a> and <a href="#" title="Terms of Use">Terms of Use</a> &copy;2017 Veterilib, Inc.
                </div>
            </div>
        </div>
    </div>
</div>
