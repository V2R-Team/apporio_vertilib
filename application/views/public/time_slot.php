<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Time Slot</title>
    <?= link_tag('assets/css/bootstrap.min.css')?>
    <?= link_tag('assets/css/font-awesome.min.css')?>
    <?= link_tag('assets/css/style.css') ?>
    
<?= link_tag('assets/css/bootstrap-reset.css') ?>
<?= link_tag('assets/css/stylesheet.css') ?>
    <?= link_tag('assets/css/bootstrap-timepicker.min.css') ?>
     <?= link_tag('assets/css/bootstrap-formhelpers.min.css') ?>
    
  </head>
  <body>
    <!-- *************** Header Start *************** -->
    <div class="header_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo_text">
              <a href="index.php" title="Vertilib">Vertilib</a>
            </div>
          </div>
<?php if(!empty($doctor_data)){?>
          <div class="col-md-8 text-right">
            <div class="profile_button">
              <div class="btn-group">
                <button type="button" class="btn btn-danger"> <i class="fa fa-user-md" aria-hidden="true"></i><?= $doctor_data['d_firstname'] ?></button>
                <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="sr-only"><i class="fa fa-bars" aria-hidden="true"></i></span>
                </button>
                <div class="dropdown-menu">
                 <a class="dropdown-item" href="<?php echo base_url()?>Account"> Dashboard </a>
                  <a class="dropdown-item" href="<?php echo base_url()?>Account/edit_profile"> Profile </a>
                  <a class="dropdown-item" href="<?php echo base_url()?>Account/appoint"> Appointments </a>
                   <a class="dropdown-item" href="<?php echo base_url() ?>Time_slot"> Addtimeslot </a>
                  <a class="dropdown-item" href="<?php echo base_url()?>Account/password"> Change Password </a>
                  <a class="dropdown-item" href="<?php echo base_url()?>Account/logout"> Sign Out </a>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>

  <?php } ?>
      </div>
    </div>
    <!-- *************** Header Close *************** -->
    <!-- *************** Main Part Start *************** -->
    <div class="singin_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <label class="profile_lable">Time Slot</label>
          </div>
        </div>
        
              

        <div class="row">


          </div>
          <br>
    <!-- *************** Main Part Start *************** -->

          <div class="col-md-12">



<?php echo form_open('Time_slot/add_time',['name'=>'add_slot']) ?>
            <div class="main_part">
              <div class="main_search_part">
                <div class="date_filter">
                  <div class="row">
                  <div class="col-md-12">
                    <div class="singup_lable1">Add Time Slot</div>
                    <div class="col-md-5">
                  <div class="input-group col-md-12"><input type="text" class="form-control" required name="date_from" placeholder="mm/dd/yyyy" id="datepicker-multiple">
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                  </div>
               </div>

                    <div class="col-md-5">
               	  <div class="input-group  col-md-12 m-b-15">
                     <div class="bootstrap-timepicker"><input id="timepicker" required type="text" name="time_from" class="form-control"/></div>
                         <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                     </div>
               </div>
               
                <div class="col-md-2">
               <button type="submit" class="btn btn-purple m-b-5 col-md-12">Add</button>
               </div>
               
               
                  </div>
 <?php echo form_close(); ?>             
                </div>
                  <div class="clear"></div>
                </div>
                <div class="col-sm-12">
                <table class="table main_search_part">
                  <tr>
                    <th>Sr.No</th>
                    <th>Date</th>
                    <th>Time Slot</th>
                    <th>Default</th>
                  </tr>
             
                <?php $id =1; if(!empty($text)):?>
                  
               <?php foreach($text as $text):?>    
                  <tr>
                    <td><?= $id++ ;?></td>
                    <td><?= $text['date_from'] ?></td>
                    <td><?= $text['time_from'] ?></td>
                  <?php  if ($text['approval'] == 0){?>
                   
                    <td><a href="<?php echo base_url();?>Time_slot/delt_time?date_from=<?= $text['date_from'] ?>&time_from=<?= $text['time_from'] ?>"><i class="fa fa-times default_icon" aria-hidden="true"></i></td>
                  <?php } else { ?> 
                    <a href="#"> <i class="fa fa-check default_icon2" aria-hidden="true"></i></a>
                   <?php } ?> 
    </tr>
                 <?php endforeach;?> 
                  <?php  else:?>
                 <tr>
                     <td colspan="4">No Record </td>  
                 </tr>
                  <?php endif;?>
                </table>
              </div>
                <div class="clear"></div>
              </div>

            </div>
            </div>


        </div>

<br><br>

    <!-- *************** Main Part Close *************** -->

 <?php include_once('footer.php');?>

    <!-- Footer Js -->
    <script src="/vertilib/assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/bootstrap-datepicker.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/bootstrap-timepicker.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/bootstrap-formhelpers.min.js" charset="utf-8"></script>
    <script>
            jQuery(document).ready(function() {
                    
                     // Time Picker
                jQuery('#timepicker').timepicker({defaultTIme: false});
                jQuery('#timepicker2').timepicker({showMeridian: false});
                jQuery('#timepicker3').timepicker({minuteStep: 15});
                
                // Date Picker
                jQuery('#datepicker').datepicker();
                jQuery('#datepicker-inline').datepicker();
                jQuery('#datepicker-multiple').datepicker({
                    numberOfMonths: 3,
                    showButtonPanel: true
                });
				});
                
        </script>
  </body>
</html>
