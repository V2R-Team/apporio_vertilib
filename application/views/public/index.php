 <!DOCTYPE html>
<html>
<head>
   
    <meta charset="utf-8">
    <link rel="icon" href="assets/images/Logo.jpg" type="image/gif" sizes="16x16">
    <title>verterilib</title>

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.css">
<script src="assets/js/jquery.min.js" charset="utf-8"></script>
<script src="assets/js/bootstrap.min.js" charset="utf-8"></script>
<script src="assets/js/jquery1.min.js" charset="utf-8"></script>
<script src="assets/js/jquery-ui.min.js" charset="utf-8"></script>

 
<style>  
     
            .ui-menu {  
                list-style:none;  
                padding: 2px;  
                margin: 0;  
                display:block;  
            }  
            .ui-menu .ui-menu {  
                margin-top: -3px;  
            }  
            .ui-menu .ui-menu-item {  
                margin:0;  
                padding: 0;  
                zoom: 1;  
                float: left;  
                clear: left;  
                width: 100%;  
                font-size:80%;  
            }  
            .ui-menu .ui-menu-item a {  
                text-decoration:none;  
                display:block;  
                padding:.2em .4em;  
                line-height:1.5;  
                zoom:1;  
            }  
            .ui-menu .ui-menu-item a.ui-state-hover,  
            .ui-menu .ui-menu-item a.ui-state-active {  
                font-weight: normal;  
                margin: -1px;  
            }  
        </style>  
</head>
<body>
  
<div class="home_header_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="logo">
                    <a href="index.php" title="Vertilib"><img src="assets/images/Logo.png" alt="vertilib-logo" title="verterilib" width="200"/></a>
                </div>
            </div>

            <div class="col-md-8 text-right">
                <div class="home_header_button">
               
            
             <?php   if($this->session->userdata('patient_id')) {?>
                   <div class="col-md-12 text-right">
            <div class="profile_button">
              <div class="btn-group">
                <button type="button" class="btn btn-danger"> <i class="fa fa-user" aria-hidden="true"></i><?= $user_data[0]['p_firstname'] ?></button>
                <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="sr-only"><i class="fa fa-bars" aria-hidden="true"></i></span>
                </button>
                <div class="dropdown-menu" >
               
                   <a class="dropdown-item" style="color:black!important;" href="<?php echo base_url();?>Useraccount"> Medical Team </a>
                  <a class="dropdown-item" style="color:black!important;" href="<?php echo base_url();?>Useraccount/user_account"> Past Appointments </a>
                  <a class="dropdown-item" style="color:black!important;"  href="<?php echo base_url();?>Useraccount/settings"> Settings </a>
                  <a class="dropdown-item" style="color:black!important;"  href="<?php echo base_url();?>Useraccount/logout"> Sign Out </a>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>

<?php } else { ?>
                  <a href="<?php echo base_url() ?>Doctorsignin">Doctor Sign</a>
                   <a href="<?php echo base_url() ?>Doctor" class="header_button_commen_one">List your practice on Veterilib</a>
                    <a href="<?php echo base_url() ?>Signin">Sign In/Join</a>
<?php }?>
  
                  
                    
                </div>
            </div>
        </div>

        <div class="row"><div class="home_header_heading">Find a doctor ? </div></div>
        <?php echo form_open('Search/doctor',['name'=>'search_data']) ?>
        <div class="row">
        <div class="col-md-3"><p></p></div>
        <div class="col-md-6 first_header_textbox" >
        <label class="label label-default" style="color:white">Select What Are You Looking For?</label>
            <?php 
            $options = array(    
        'specialty'         => 'Specialty',
        'animal'           => 'Animal',
        'symptoms'         => 'Symptoms',
        'doctor'        => 'Doctor Name',
        'clinic'        => 'Clinic Name',
       
               
);
            $js = 'class="header_textbox" id="search_from" placeholder="plz select this "';
            echo form_dropdown('search_from', $options, '',$js );
            ?>
            </div>
            <div class="col-md-3"><p></p></div>
            <div class="col-md-4 first_header_textbox" >
            
                <?php echo form_input(['name'=>'specialty','class'=>'header_textbox','placeholder'=>'Specialty,Animal,Clinic Name,Symptoms','id'=>'search'])?>
             
               

            </div>
            <div class="col-md-4 padding_none header_textbox_one">
                <?php echo form_input(['name'=>'city','class'=>'header_textbox','placeholder'=>'City','id'=>'search1'])?>


</div>
            <div class="col-md-4 padding_none header_textbox_one">
               
               <input type="text" id= "datepicker" class="header_textbox" name="date" value="" placeholder="Date" style="margin-left:2%;" required data-readonly />
           <button type="submit" name="search" class="header_button" style="margin-right:-2%;"><i class="fa fa-search" aria-hidden="true" ></i></button>

</div>
                    
            
        </div>
        <?php echo form_close(); ?>
    </div>
</div>  
<!-- *************** Home Page Header Close *************** -->
<script>  
$(document).ready(function() {
    $("#datepicker").datepicker().attr('readOnly', 'true');;
  });
</script>
<script type="text/javascript">  
        $(this).ready( function() { 
        
     var search_data = '';
            $("#search").autocomplete({  
  
           //search_data = $("#search_from").val() ;
            minLength: 1,  
                source:   
                function(req, add){  
                    $.ajax({  
                        url: "<?php echo base_url(); ?>Search/lookup",  
                        dataType: 'json',  
                        type: 'POST',    
                        data: 'search_from='+$("#search_from").val()+'&specialty='+$("#search").val(),  
                        success:      
                        function(data){  
                            if(data.response == "true"){ 
                                add(data.message);  
                                console.log(data);
                            }  
                        },  
                    });  
                },  
                     
            });  
        });  
</script>
<script type="text/javascript">  
        $(this).ready( function() {  
            $("#search1").autocomplete({  
 
                minLength: 1,  
                source:   
                function(req, add){  
                    $.ajax({  
                        url: "<?php echo base_url(); ?>Search/look",  
                        dataType: 'json',  
                        type: 'POST',  
                        data: 'city='+$("#search1").val(),  
                        success:      
                        function(data){  
                            if(data.response == "true"){  
                                add(data.message);  
                                console.log(data);
                            }  
                        },  
                    });  
                },  
                     
            });  
        });  
</script>
<?php include_once('footer.php');?>
<!-- Footer Js -->

</body>
</html>

