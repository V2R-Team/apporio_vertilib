<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Vertilib</title>
    <?= link_tag('assets/css/bootstrap.min.css')?>
    <?= link_tag('assets/css/font-awesome.min.css')?>
    <?= link_tag('assets/css/style.css') ?>
    <?= link_tag('assets/css/fullcalendar.min.css');?>
    <script src="/vertilib/assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/moment.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/fullcalendar.min.js" charset="utf-8"></script>
    
  </head>
  <body>
    <!-- *************** Header Start *************** -->
    <div class="header_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo_text">
              <a href="<?php echo base_url() ?>" title="Vertilib">Vertilib</a>
            </div>
          </div>
<?php if(!empty($doctor_data)){?>

          <div class="col-md-8 text-right">
            <div class="profile_button">
              <div class="btn-group">
                <button type="button" class="btn btn-danger"> <i class="fa fa-user-md" aria-hidden="true"></i> <?= $doctor_data['d_firstname'] ?></button>
                <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="sr-only"><i class="fa fa-bars" aria-hidden="true"></i></span>
                </button>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="<?php echo base_url()?>Account"> Dashboard </a>
                  <a class="dropdown-item" href="<?php echo base_url()?>Account/edit_profile"> Profile </a>
                  <a class="dropdown-item" href="<?php echo base_url()?>Account/appoint"> Appointments </a>
                   <a class="dropdown-item" href="<?php echo base_url() ?>Time_slot"> Addtimeslot </a>
                  <a class="dropdown-item" href="<?php echo base_url()?>Account/password"> Change Password </a>
                  <a class="dropdown-item" href="<?php echo base_url()?>Account/logout"> Sign Out </a>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <?php } ?>


      </div>
    </div>
    <!-- *************** Header Close *************** -->


    <!-- *************** Main Part Start *************** -->
    <div class="singin_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <label class="profile_lable">Change Password</label>
          </div>
        </div>

        <br><br>


        <div class="row">
           <?php if(!empty($doctor_data)){?>
          <div class="col-md-4">
        <div class="book_docter_details" style="margin-top:0px !important;">
          <div class="book_docter_details_lable">Profile</div>
          <div class="doctor_details">
            <div class="">
              <div class="col-md-4">
                <div class="book_doctor_img"> <img src=<?= $doctor_data['d_image'] ?> alt="" width="80"/> </div>
              </div>
              <div class="col-md-8">
                <label class="book_doctor_name">Dr.<?= $doctor_data['d_firstname'] ?> <?= $doctor_data['d_lastname'] ?></label>
                <div class="book_doctor_reating"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> </div>
                <div class="book_doctor_text">
                  <p><?= $doctor_data['d_city'] ?> - <?= $doctor_data['zipcode'] ?></p>
                </div>
              </div>
              <div class="clear"></div>
            </div>
            
            <!-- <div class="book_docter_details_lable2 border_book_docter_details">Tuesday, December 27 - 11:45 AM</div> -->
            
            <div class="book_docter_details_lable2 book_docter_details_border"> Email <br>
              <span><?= $doctor_data['d_email'] ?></span> </div>
            <div class="book_docter_details_lable2 book_docter_details_border"> Call <br>
              <span>+91 - <?= $doctor_data['d_phone'] ?></span> </div>
            <div class="book_docter_details_lable2 book_docter_details_border"> Specialty <br>
              <span><?= $doctor_data['d_specialty'] ?></span> </div>
            <div class="book_docter_details_lable2"> Clinic Name <br>
              <span><?= $doctor_data['d_clinic'] ?></span> </div>
          </div>
        </div>
      </div>
      <?php } ?>    
    <?php echo form_open('Account/change_password',['name'=>'password']) ?>      
          <div class="col-md-8">
          <div class="row tab_row">
            <?php  if (isset($error)){
    echo "<div class='error' style=color:red;>$error</div>";
}?>
                <div class="col-md-12">
                  <div class="form-group"><h4>Enter your current password</h4>
 <?php echo form_password(['name'=>'oldpassword','class'=>'form-control','placeholder'=>'Enter Your Old Password','value'=>'','required'=>'required'])?>
  <?php echo "<div class='error'>".form_error('oldpassword')."</div>";?>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group"><h4>Choose a Password</h4>
    <?php echo form_password(['name'=>'d_password','class'=>'form-control','placeholder'=>'Choose a Password','value'=>'','required'=>'required'])?>
               <?php echo "<div class='error'>".form_error('d_password')."</div>";?>     
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group"><h4>Confirm your Password</h4>
 <?php echo form_password(['name'=>'cd_password','class'=>'form-control','placeholder'=>'Confirm your Password','value'=>'','required'=>'required'])?>
               <?php echo "<div class='error'>".form_error('cd_password')."</div>";?>      
                  </div>
                </div>
              </div>
 <?php echo form_submit(['name'=>'submit','class'=>'btn btn-info btn-lg m-b-5','value'=>'Save'])?>
          </div>
        </div>
<?php echo form_close(); ?>

      </div>
    </div>
    <!-- *************** Main Part Close *************** -->
 <?php include_once ('footer.php'); ?>

    <script>

    	$(document).ready(function() {

    		$('#calendar').fullCalendar({
    			header: {
    				left: 'prev,next today',
    				center: 'title',
    				right: 'month,basicWeek,basicDay'
    			},
    			defaultDate: '2016-12-12',
    			navLinks: true, // can click day/week names to navigate views
    			editable: true,
    			eventLimit: true, // allow "more" link when too many events
    			events: [
    				{
    					title: 'All Day Event',
    					start: '2016-12-01'
    				},
    				{
    					title: 'Long Event',
    					start: '2016-12-07',
    					end: '2016-12-10'
    				},
    				{
    					id: 999,
    					title: 'Repeating Event',
    					start: '2016-12-09T16:00:00'
    				},
    				{
    					id: 999,
    					title: 'Repeating Event',
    					start: '2016-12-16T16:00:00'
    				},
    				{
    					title: 'Conference',
    					start: '2016-12-11',
    					end: '2016-12-13'
    				},
    				{
    					title: 'Meeting',
    					start: '2016-12-12T10:30:00',
    					end: '2016-12-12T12:30:00'
    				},
    				{
    					title: 'Lunch',
    					start: '2016-12-12T12:00:00'
    				},
    				{
    					title: 'Meeting',
    					start: '2016-12-12T14:30:00'
    				},
    				{
    					title: 'Happy Hour',
    					start: '2016-12-12T17:30:00'
    				},
    				{
    					title: 'Dinner',
    					start: '2016-12-12T20:00:00'
    				},
    				{
    					title: 'Birthday Party',
    					start: '2016-12-13T07:00:00'
    				},
    				{
    					title: 'Click for Google',
    					url: 'http://google.com/',
    					start: '2016-12-28'
    				}
    			]
    		});

        $('#toggle_event_editing button').click(function(){
        	/* reverse locking status */
        	$('#toggle_event_editing button').eq(0).toggleClass('locked_inactive locked_active btn-default btn-info');
        	$('#toggle_event_editing button').eq(1).toggleClass('unlocked_inactive unlocked_active btn-info btn-default');
        });

    	});

    </script>
  </body>
</html>
