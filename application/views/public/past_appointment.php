<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Veterilib </title>
<?= link_tag('assets/css/bootstrap.min.css')?>
<?= link_tag('assets/css/font-awesome.min.css')?>
<?= link_tag('assets/css/style.css') ?>
<?= link_tag('assets/css/bootstrap-reset.css') ?>
<?= link_tag('assets/css/stylesheet.css') ?>
<script src="/vertilib/assets/js/jquery.min.js" charset="utf-8"></script>
<script src="/vertilib/assets/js/bootstrap.min.js" charset="utf-8"></script>

</head>
<body>
<!-- *************** Header Start *************** -->
<div class="header_bg">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="logo_text"> <a href="<?php echo base_url();?>" title="Vertilib">Veterilib </a> </div>
      </div>
      <?php if(!empty($user_data)){?>
      <div class="col-md-8 text-right">
        <div class="profile_button">
          <div class="btn-group">
            <button type="button" class="btn btn-danger"> <i class="fa fa-user" aria-hidden="true"></i>
            <?= $user_data[0]['p_firstname'] ?>
            </button>
            <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="sr-only"><i class="fa fa-bars" aria-hidden="true"></i></span> </button>
            <div class="dropdown-menu"> <a class="dropdown-item" href="<?php echo base_url();?>Useraccount"> Medical Team </a> <a class="dropdown-item" href="<?php echo base_url();?>Useraccount/user_account"> Past Appointments </a> <a class="dropdown-item" href="<?php echo base_url();?>Useraccount/settings"> Settings </a> <a class="dropdown-item" href="<?php echo base_url();?>Useraccount/logout"> Sign Out </a> </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
    <?php } ?>
  </div>
</div>
<!-- *************** Header Close *************** --> 

<!-- *************** Main Part Start *************** -->
<div class="singin_bg1" style="margin:5px 0px 20px 0px;">
  <div class="container">
    <div class="profile_tab">
      <ul>
        <li style="margin-left:0px;"><a href="<?php echo base_url();?>Useraccount">
          <h3 style="padding-left:0px; border:none;" class="title">Medical Team</h3>
          </a></li>
        <li><a href="<?php echo base_url();?>Useraccount/user_account" class="active">
          <h3 class="title">Past Appointments and Reviews</h3>
          </a></li>
        <li><a href="<?php echo base_url();?>Useraccount/settings">
          <h3 class="title">Settings</h3>
          </a></li>
      </ul>
    </div>
      
    <div class="row">
   <?php if(!empty($app_data)){?>
        
  <?php foreach($app_data as $app_data):?>      
      <div class="col-md-4">
       
<div class="book_docter_details">
          <div class="book_docter_details_lable">Profile 
          <button class="fa fa-close btn btn-primary" data-toggle="modal" data-target="#myModal"  style=" float:right; cursor:pointer; height:26px; padding:0px 5px;"></button>
          </div>
          <div class="doctor_details">
            <div class="">
              <div class="col-md-4">
                <div class="book_doctor_img"> <img src=<?= $app_data['d_image'] ?> class="img-circle" alt="" width="80"/> </div>
              </div>
              <div class="col-md-8">
                <label class="book_doctor_name">Dr.<?= $app_data['d_firstname'] ?> <?= $app_data['d_lastname'] ?></label>
                <div class="book_doctor_reating"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> </div>
                <div class="book_doctor_text">
                  <p><?= $app_data['d_city'] ?>- <?= $app_data['zipcode'] ?></p>
                </div>
              </div>
              <div class="clear"></div>
            </div>
            
            <!-- <div class="book_docter_details_lable2 border_book_docter_details">Tuesday, December 27 - 11:45 AM</div> -->
            
           <div class="book_docter_details_lable2 book_docter_details_border"> Email <br>
              <span><?= $app_data['d_email'] ?></span> </div>
            <div class="book_docter_details_lable2 book_docter_details_border"> Call <br>
              <span><?= $app_data['d_phone'] ?></span> </div>
            <div class="book_docter_details_lable2 book_docter_details_border"> Specialty <br>
              <span><?= $app_data['d_specialty'] ?></span> </div>
            <div class="book_docter_details_lable2"> Clinic Name <br>
              <span><?= $app_data['d_clinic'] ?></span> </div>
          </div>
        </div>
	   
      </div>
      <?php endforeach; ?>
       <?php } ?>
      
     
    </div>
  </div>
</div>
<!-- *************** Main Part Close *************** -->

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myModalLabel">Cancel Your Appointment</h4>
                                            </div>
                                            <div class="modal-body">
                                                <h4>Are You Sure ??</h4>
                                                <p>you want to chancel ur appointment</p>
                                               
                                                
                                            </div>
                                            <div class="modal-footer">
                                                
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                               
                                                <a href="<?php echo base_url();?>Chancel_appointment/delt_appoint?app_id=<?= $app_data['app_id'] ?>&slot_id=<?= $app_data['slot_id'] ?>" class="btn btn-default" role="button">Chancel</a>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div>


<?php include_once ('footer.php'); ?>

</body>
</html>
