<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">

<title>Veterilib </title>
<?= link_tag('assets/css/bootstrap.min.css')?>
<?= link_tag('assets/css/font-awesome.min.css')?>
<?= link_tag('assets/css/style.css') ?>
<?= link_tag('assets/css/bootstrap-reset.css') ?>
<?= link_tag('assets/css/stylesheet.css') ?>
<?= link_tag('assets/css/fullcalendar.min.css');?>
<script src="/vertilib/assets/js/jquery.min.js" charset="utf-8"></script>
<script src="/vertilib/assets/js/bootstrap.min.js" charset="utf-8"></script>
<script src="/vertilib/assets/js/moment.min.js" charset="utf-8"></script>
<script src="/vertilib/assets/js/fullcalendar.min.js" charset="utf-8"></script>
</head>
<body>
<!-- *************** Header Start *************** -->
<div class="header_bg">
  <div class="container">
    <div class="row">
      <div class="col-md-4"> 
        <div class="logo_text"> <a href="<?php echo base_url();?>" title="Vertilib">Veterilib </a> </div>
      </div>
  <?php if(!empty($user_data)){?>   
      <div class="col-md-8 text-right">
        <div class="profile_button">
          <div class="btn-group">
            <button type="button" class="btn btn-danger"> <i class="fa fa-user" aria-hidden="true"> <?= $user_data[0]['p_firstname'] ?></i>
            
            </button>
            <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="sr-only"><i class="fa fa-bars" aria-hidden="true"></i></span></button>
            <div class="dropdown-menu"> <a class="dropdown-item" href="<?php echo base_url();?>Useraccount"> Medical Team </a> <a class="dropdown-item" href="<?php echo base_url();?>Useraccount/user_account"> Past Appointments </a> <a class="dropdown-item" href="<?php echo base_url();?>Useraccount/settings"> Settings </a> <a class="dropdown-item" href="<?php echo base_url();?>Useraccount/logout"> Sign Out </a> </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
   
  </div>
</div>
<!-- *************** Header Close *************** --> 

<!-- *************** Main Part Start *************** -->
<div class="singin_bg1" style="margin:5px 0px 20px 0px;">
  <div class="container">
    <div class="profile_tab">
      <ul>
        <li style="margin-left:0px;"><a href="<?php echo base_url();?>Useraccount">
          <h3 style="padding-left:0px; border:none;" class="title">Medical Team</h3>
          </a></li>
        <li><a href="<?php echo base_url();?>Useraccount/user_account">
          <h3 class="title">Past Appointments and Reviews</h3>
          </a></li>
        <li><a href="<?php echo base_url();?>Useraccount/settings" class="active">
          <h3 class="title">Settings</h3>
          </a></li>
      </ul>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="tabs-vertical-env">
        <?php if(isset($tab_val)){ ?>
        <script>
         $(this).ready( function() { 
           $( "li" ).removeClass( "active" );
            $( "#profile" ).removeClass( "active" );
           $( "#<?php echo $tab_val; ?>_li" ).addClass( "active" );
            $( "#<?php echo $tab_val; ?>" ).attr("class","tab-pane active");
             $("a").attr("aria-expanded","false");
              $("#<?php echo $tab_val; ?>_a").attr("aria-expanded","true");
             });
        </script>
        <?php } ?>
          <ul class="nav tabs-vertical col-md-3">
            <li class="active" id="profile_li"> <a href="#profile" id="profile_a" data-toggle="tab" aria-expanded="true">Profile</a> </li>
            <li class="" id="password_li"> <a href="#password" id="password_a" data-toggle="tab" aria-expanded="false">Password</a> </li>
            <li class=""> <a href="#notification" data-toggle="tab" aria-expanded="false">Notification Settings</a> </li>
            <li class=""> <a href="#insurance" data-toggle="tab" aria-expanded="false">Insurance</a> </li>
            <li class=""> <a href="#demographic" data-toggle="tab" aria-expanded="false">Demographic Info</a> </li>
            <li class=""> <a href="#authorizations" data-toggle="tab" aria-expanded="false">Authorizations</a> </li>
          </ul>
         
          <div class="tab-content col-md-9">
            <div class="tab-pane active" id="profile">
  <?php echo form_open('Userprofile/update',['name'=>'update']) ?>          
              <div class="row">
                <h3>My Profile</h3>
                 <?php  if (isset($success)){
    echo "<div class='success' style=color:blue;>$success</div>";
}?>
              </div>
           
              <div class="row tab_row">
                <div class="col-md-4">
                  <div class="form-group"><h4>First Name</h4>
                    <input type="text" class="form-control" placeholder="" name="p_firstname" value="<?= $user_data[0]['p_firstname'] ?>" id="" required>
                  </div>
                </div>
                
                <div class="col-md-4">
                  <div class="form-group"><h4>Last Name</h4>
                    <input type="text" class="form-control" placeholder="" name="p_lastname" value="<?= $user_data[0]['p_lastname'] ?>" id="" required>
                  </div>
                </div>
                
              </div>
              <div class="row tab_row">
                <div class="col-md-5">
                  <div class="form-group"><h4>Email</h4>
                    <input type="text" class="form-control" placeholder="Your Email" name="p_email" value="<?= $user_data[0]['p_email'] ?>" id="email" required>
                  </div>
                </div>
              </div>
              <div class="row tab_row">
                <div class="col-md-4">
                  <div class="form-group"><h4>Phone Number</h4>
                    <input type="text" class="form-control" placeholder="" name="p_phone" value="<?= $user_data[0]['p_phone'] ?>" id="" >
                  </div>
                </div>
                
              </div>
              <div class="row tab_row">
                <div class="col-md-4">
                  <div class="form-group"><h4>Address</h4>
                    <input type="text" class="form-control" placeholder="Street Address" name="s_address" value="<?= $user_data[0]['s_address'] ?>" id="" >
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group"><h4 style="visibility:hidden;">Apt, Ste, Unit #</h4>
                    <input type="text" class="form-control" placeholder="Apt, Ste, Unit #" name="a_address" value="<?= $user_data[0]['a_address'] ?>" id="" >
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group"><h4 style="visibility:hidden;">city</h4>
                    <input type="text" class="form-control" placeholder="city" name="p_city" value="<?= $user_data[0]['p_city'] ?>" id="" >
                  </div>
                </div>
               
                
                
              </div>
              <div class="row tab_row">
                <div class="col-md-4">
                  <div class="form-group"><h4>Gender</h4>
                    <select class="form-control" name="p_sex" id="" required="">
                      <option>
                      <?= $user_data[0]['p_sex'] ?>
                      </option>
                      <option id="1" value="1">Male</option>
                      <option id="1" value="1">Female</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row tab_row">
                <div class="col-md-4">
                  <div class="form-group"><h4>Date of Birth</h4>
                    <input type="date" class="form-control" placeholder="Zip Code" name="p_dob" value="<?= $user_data[0]['p_dob'] ?>" id="" >
                  </div>
                </div>
              </div>
       <?php echo form_submit(['name'=>'submit','class'=>'btn btn-info btn-lg m-b-5','value'=>'Save'])?>
              
            </div>
            <?php echo form_close(); ?>
        <?php } ?>  

            <div class="tab-pane" id="password">
              <div class="row">
                <h3>Password</h3>
              </div>
   <?php  if (isset($error)){
    echo "<div class='error' style=color:red;>$error</div>";
}?>
<?php echo form_open('Useraccount/change_password',['name'=>'password']) ?>  
              <div class="row tab_row">
                <div class="col-md-12">
                  <div class="form-group"><h4>Enter your current password</h4>
 <?php echo form_password(['name'=>'oldpassword','class'=>'form-control','placeholder'=>'Enter Your Old Password','value'=>'','required'=>'required'])?>
  <?php echo "<div class='error'>".form_error('oldpassword')."</div>";?>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group"><h4>Choose a Password</h4>
    <?php echo form_password(['name'=>'p_password','class'=>'form-control','placeholder'=>'Choose a Password','value'=>'','required'=>'required'])?>
               <?php echo "<div class='error'>".form_error('p_password')."</div>";?>     
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group"><h4>Confirm your Password</h4>
 <?php echo form_password(['name'=>'cp_password','class'=>'form-control','placeholder'=>'Confirm your Password','value'=>'','required'=>'required'])?>
               <?php echo "<div class='error'>".form_error('cp_password')."</div>";?>      
                  </div>
                </div>
              </div>
 <?php echo form_submit(['name'=>'submit','class'=>'btn btn-info btn-lg m-b-5','value'=>'Save'])?>
              
              
            </div>
 <?php echo form_close(); ?>
            
            
            <div class="tab-pane" id="notification">
              <div class="row">
                <h3>Notification</h3>
              </div>
              <div class="row tab_row">
                <div class="col-md-5">
                  <div class="form-group">
                    <h4>Email</h4>
                    <label class="cr-styled">
                       <input type="checkbox">
                       <i class="fa"></i> 
                       Check me out !
                    </label>
                  </div>
                </div>
              </div>
              
              <div class="row tab_row">
                <div class="col-md-5">
                  <div class="form-group">
                    <h4>App Settings</h4>
                    <p>Download the Zocdoc <a href="#">Android App</a> or <a href="#">iPhone App</a>.</p>
                    <label style="margin-top:8px;">
                       <input type="checkbox">
                       <i class="fa"></i> 
                       Push notify appointment reminders
                    </label>
                    <br>
                    <label style="margin-top:8px;">
                       <input type="checkbox">
                       <i class="fa"></i> 
                       Push notify appointment reminders
                    </label>
                    <br>
                    <label style="margin-top:8px;">
                       <input type="checkbox">
                       <i class="fa"></i> 
                       Push notify appointment reminders
                    </label>
                  </div>
                </div>
              </div>
              
              <button type="button" class="btn btn-info btn-lg m-b-5">Save</button>
              <button type="button" class="btn btn-white btn-lg" >Cancel</button>
              
            </div>
            
            
            <div class="tab-pane" id="insurance">
              <div class="row">
                <h3>Insurance</h3>
              </div>
              <div class="row tab_row">
                <div class="col-md-4">
                  <div class="form-group"><h4>Medical Insurance</h4>
                    <select class="form-control" name="" id="" required="">
                      <option>--Select Medical Insurance--</option>
                      <option id="1" value="1">Medical Insurance 1</option>
                      <option id="1" value="1">Medical Insurance 2</option>
                      <option id="1" value="1">Medical Insurance 3</option>
                    </select>
                  </div>
                </div>
              </div>
              
              <div class="row tab_row">
                <div class="col-md-4">
                  <div class="form-group"><h4>Dental Insurance</h4>
                    <select class="form-control" name="" id="" required="">
                      <option>--Select Dental Insurance--</option>
                      <option id="1" value="1">Dental Insurance 1</option>
                      <option id="1" value="1">Dental Insurance 2</option>
                      <option id="1" value="1">Dental Insurance 3</option>
                    </select>
                  </div>
                </div>
              </div>
              
              <div class="row tab_row">
                <div class="col-md-4">
                  <div class="form-group"><h4>Vision Insurance</h4>
                    <select class="form-control" name="" id="" required="">
                      <option>--Select Vision Insurance--</option>
                      <option id="1" value="1">Vision Insurance 1</option>
                      <option id="1" value="1">Vision Insurance 2</option>
                      <option id="1" value="1">Vision Insurance 3</option>
                    </select>
                  </div>
                </div>
              </div>
              
              <button type="button" class="btn btn-info btn-lg m-b-5">Save</button>
              <button type="button" class="btn btn-white btn-lg" >Cancel</button>
              
            </div>
            <div class="tab-pane" id="demographic">
              <div class="row">
                <h3>Demographic</h3>
              </div>
              
              <div class="row tab_row">
                <div class="col-md-5">
                  <div class="form-group">
                    <h4>Race Select one or more</h4>
                    <label style="margin-top:8px;">
                       <input type="checkbox">
                       <i class="fa"></i> 
                       American Indian or Alaska Native
                    </label>
                    <br>
                    <label style="margin-top:8px;">
                       <input type="checkbox">
                       <i class="fa"></i> 
                       Asian
                    </label>
                    <br>
                    <label style="margin-top:8px;">
                       <input type="checkbox">
                       <i class="fa"></i> 
                       Black or African American
                    </label>
                    <br>
                    <label style="margin-top:8px;">
                       <input type="checkbox">
                       <i class="fa"></i> 
                       Native Hawaiian
                    </label>
                    <br>
                    <label style="margin-top:8px;">
                       <input type="checkbox">
                       <i class="fa"></i> 
                       Other Pacific Islander
                    </label>
                    <br>
                    <label style="margin-top:8px;">
                       <input type="checkbox">
                       <i class="fa"></i> 
                       White
                    </label>
                    <br>
                    <label style="margin-top:8px;">
                       <input type="checkbox">
                       <i class="fa"></i> 
                       Other
                    </label>
                    <br>
                    <label style="margin-top:8px;">
                       <input type="checkbox">
                       <i class="fa"></i> 
                       Decline to Answer
                    </label>
                  </div>
                </div>
              </div>
              
              
              <div class="row tab_row">
                <div class="col-md-5">
                  <div class="form-group">
                    <h4>Ethnicity</h4>
                    <label style="margin-top:8px;">
                       <input type="checkbox">
                       <i class="fa"></i> 
                       Hispanic or Latino
                    </label>
                    <br>
                    <label style="margin-top:8px;">
                       <input type="checkbox">
                       <i class="fa"></i> 
                       Not Hispanic or Latino
                    </label>
                    <br>
                    <label style="margin-top:8px;">
                       <input type="checkbox">
                       <i class="fa"></i> 
                       Decline to Answer
                    </label>
                  </div>
                </div>
              </div>
              
              <div class="row tab_row">
                <div class="col-md-6">
                  <div class="form-group"><h4>Preferred Language</h4>
                  <p>Zocdoc is currently available in <a href="#">English</a> and <a href="#">Español</a>.</p>
                    <select class="form-control" name="" id="" required="">
                      <option>--Select Launguage--</option>
                      <option id="1" value="1">English</option>
                      <option id="1" value="1">Arabic</option>
                    </select>
                  </div>
                </div>
              </div>
              
              
              <div class="row tab_row">
                <div class="col-md-6">
                  <div class="form-group"><h4>Zip(Optional)</h4>
                    <input type="text" class="form-control" placeholder="" name="" value="" id="" required>
                  </div>
                </div>
              </div>
              
               <button type="button" class="btn btn-info btn-lg m-b-5">Save</button>
              <button type="button" class="btn btn-white btn-lg" >Cancel</button>
              
              
            </div>
            
            
            
            
            <div class="tab-pane" id="authorizations">
              <div class="row">
                <h3>Authorizations</h3>
              </div>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- *************** Main Part Close *************** -->

<?php include_once ('footer.php'); ?>
<script>

    	$(document).ready(function() {

    		$('#calendar').fullCalendar({
    			header: {
    				left: 'prev,next today',
    				center: 'title',
    				right: 'month,basicWeek,basicDay'
    			},
    			defaultDate: '2016-12-12',
    			navLinks: true, // can click day/week names to navigate views
    			editable: true,
    			eventLimit: true, // allow "more" link when too many events
    			events: [
    				{
    					title: 'All Day Event',
    					start: '2016-12-01'
    				},
    				{
    					title: 'Long Event',
    					start: '2016-12-07',
    					end: '2016-12-10'
    				},
    				{
    					id: 999,
    					title: 'Repeating Event',
    					start: '2016-12-09T16:00:00'
    				},
    				{
    					id: 999,
    					title: 'Repeating Event',
    					start: '2016-12-16T16:00:00'
    				},
    				{
    					title: 'Conference',
    					start: '2016-12-11',
    					end: '2016-12-13'
    				},
    				{
    					title: 'Meeting',
    					start: '2016-12-12T10:30:00',
    					end: '2016-12-12T12:30:00'
    				},
    				{
    					title: 'Lunch',
    					start: '2016-12-12T12:00:00'
    				},
    				{
    					title: 'Meeting',
    					start: '2016-12-12T14:30:00'
    				},
    				{
    					title: 'Happy Hour',
    					start: '2016-12-12T17:30:00'
    				},
    				{
    					title: 'Dinner',
    					start: '2016-12-12T20:00:00'
    				},
    				{
    					title: 'Birthday Party',
    					start: '2016-12-13T07:00:00'
    				},
    				{
    					title: 'Click for Google',
    					url: 'http://google.com/',
    					start: '2016-12-28'
    				}
    			]
    		});

        $('#toggle_event_editing button').click(function(){
        	/* reverse locking status */
        	$('#toggle_event_editing button').eq(0).toggleClass('locked_inactive locked_active btn-default btn-info');
        	$('#toggle_event_editing button').eq(1).toggleClass('unlocked_inactive unlocked_active btn-info btn-default');
        });

    	});

    </script>
</body>
</html>
