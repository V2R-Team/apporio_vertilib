<!DOCTYPE html>
<?php include_once('header.php');?>

    <!-- *************** Main Part Start *************** -->
    <div class="singin_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="col-md-4">
            <label class="singin_lable">Doctor Sign in </label>
          </div>
          <div class="col-md-4"></div>
        </div>
<?php echo form_open('Doctorsignin/login_user',['name'=>'doc_login']) ?>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="col-md-4">
            <div class="">
              <div class="singin_lable2">Sign into your vertilib account</div>
               <?php  if (isset($error)){
    echo "<div class='error' style=color:red;>$error</div>";
}?>
 <?php echo form_input(['name'=>'d_email','class'=>'sign_textbox','placeholder'=>'Email Adddress','value'=>set_value('d_email')])?>
              <?php echo "<div class='error'>".form_error('d_email')."</div>";?>              
<?php echo form_password(['name'=>'d_password','class'=>'sign_textbox','placeholder'=>'Enter Password'])?>
              <?php echo "<div class='error'>".form_error('d_password')."</div>";?>
  <?php echo form_submit(['name'=>'submit','class'=>'sign_button','value'=>'Sign in'])?>    
              
              <div class="">
               
         <?php echo form_close(); ?>        
                <div class="clearfix"></div>
              </div>

            </div>
          </div>
          <div class="col-md-4"></div>

            
        </div>
      </div>
    </div>
   

   

   <?php include_once('footer.php');?>

   <script src="assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="assets/js/bootstrap.min.js" charset="utf-8"></script>
  </body>
</html>

