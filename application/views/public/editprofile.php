<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Vertilib</title>
    <?= link_tag('assets/css/bootstrap.min.css')?>
    <?= link_tag('assets/css/font-awesome.min.css')?>
    <?= link_tag('assets/css/style.css') ?>
    <?= link_tag('assets/css/fullcalendar.min.css');?>
     <?= link_tag('assets/css/jquery-ui.css');?>
    
    <script src="/vertilib/assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/moment.min.js" charset="utf-8"></script>
    
 <script src="/vertilib/assets/js/jquery1.min.js" charset="utf-8"></script>
<script src="/vertilib/assets/js/jquery-ui.min.js" charset="utf-8"></script>
  </head>
  <body>
    <!-- *************** Header Start *************** -->
    <div class="header_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo_text">
              <a href="<?php echo base_url() ?>" title="Vertilib">Vertilib</a>
            </div>
          </div>
<?php if(!empty($doctor_data)){?>
          <div class="col-md-8 text-right">
            <div class="profile_button">
              <div class="btn-group">
                <button type="button" class="btn btn-danger"> <i class="fa fa-user-md" aria-hidden="true"></i><?= $doctor_data['d_firstname'] ?></button>
                <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="sr-only"><i class="fa fa-bars" aria-hidden="true"></i></span>
                </button>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="<?php echo base_url()?>Account"> Dashboard </a>
                  <a class="dropdown-item" href="<?php echo base_url()?>Account/edit_profile"> Profile </a>
                  <a class="dropdown-item" href="<?php echo base_url()?>Account/appoint"> Appointments </a>
                   <a class="dropdown-item" href="<?php echo base_url() ?>Time_slot"> Addtimeslot </a>
                  <a class="dropdown-item" href="<?php echo base_url()?>Account/password"> Change Password </a>
                  <a class="dropdown-item" href="<?php echo base_url()?>Account/logout"> Sign Out </a>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
 <?php } ?>

      </div>
    </div>
    <!-- *************** Header Close *************** -->


    <!-- *************** Main Part Start *************** -->
    <div class="singin_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <label class="profile_lable">Profile</label>
          </div>
        </div>

        <br><br>



        <div class="row">
          <div class="col-md-4">
            <div class="dashboard-text">

              <div class="book_docter_details" style="margin-top:0px;">
                <div class="book_docter_details_lable">Profile</div>

                <div class="doctor_details">
                  <div class="">
                    <div class="col-md-4">
                      <div class="book_doctor_img">
                        <img src=<?= $doctor_data['d_image'] ?> alt="" width="80"/>
                      </div>
                    </div>
                    <div class="col-md-8">
                      <label class="book_doctor_name">Dr. <?= $doctor_data['d_firstname'] ?> <?= $doctor_data['d_lastname'] ?></label>
                      <div class="book_doctor_reating">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                      </div>
                      <div class="book_doctor_text">
                        <p><?= $doctor_data['d_city'] ?> - <?= $doctor_data['zipcode'] ?></p>
                      </div>
                    </div>
                    <div class="clear"></div>
                  </div>

                  <!-- <div class="book_docter_details_lable2 border_book_docter_details">Tuesday, December 27 - 11:45 AM</div> -->

                  <div class="book_docter_details_lable2 book_docter_details_border">
                    Email  <br>
                    <span><?= $doctor_data['d_email'] ?></span>
                  </div>

                  <div class="book_docter_details_lable2 book_docter_details_border">
                    Call  <br>
                    <span><?= $doctor_data['d_phone'] ?></span>
                  </div>

                  <div class="book_docter_details_lable2 book_docter_details_border">
                    Specialty  <br>
                    <span><?= $doctor_data['d_specialty'] ?></span>
                  </div>

                  <div class="book_docter_details_lable2">
                    Clinic Name  <br>
                    <span><?= $doctor_data['d_clinic'] ?></span>
                  </div>

                </div>
              </div>

            </div>
          </div>
          <div class="col-md-8">
            <!-- <div class="dashboard-heading dashboard_other_heading"> Edit Profile</div> -->
            <br>
            <?php  if (isset($success)){
    echo "<div class='success' style=color:blue;>$success</div>";
}?>
            <div class="edit_profile_div">
              <label class="edit_profile_label">Name</label>
              <div class="edit_profile_text">
<?php echo form_input(['name'=>'firstname','class'=>'edit_profile_textbox','value'=>$doctor_data['d_firstname'].  $doctor_data['d_lastname'],"readonly"=>"readonly"])?>
               
              </div>
            </div>
 <?php echo form_open('Account/edit_profile_data',['name'=>'edit_profile']) ?>
            <div class="edit_profile_div">
              <label class="edit_profile_label">Email</label>
              <div class="edit_profile_text">
<?php echo form_input(['name'=>'d_email','class'=>'edit_profile_textbox','value'=>$doctor_data['d_email'] ])?>
               
              </div>
            </div>

            <div class="edit_profile_div">
              <div class="edit_profile_text col-md-4">
                <label class="edit_profile_label">Cell</label>
                <?php echo form_input(['name'=>'d_phone','class'=>'edit_profile_textboxfull','value'=>$doctor_data['d_phone']])?>
                
              </div>
      

            <div class="edit_profile_div">
              <label class="edit_profile_label">Address</label>
              <div class="edit_profile_text col-md-4">
               <?php echo form_input(['name'=>'ds_address','class'=>'edit_profile_textboxfull','value'=>$doctor_data['ds_address'],'placeholder'=>'Street Address'])?>
                
              </div>
              <div class="edit_profile_text col-md-4">
              <?php echo form_input(['name'=>'da_address','class'=>'edit_profile_textboxfull','value'=>$doctor_data['da_address'],'placeholder'=>'Apt, Ste, Unit #'])?>
                
              </div>
              <div class="edit_profile_text col-md-4">
                  <select class="edit_profile_textboxfull" name="d_city">
                      <option value="<?= $doctor_data['city_id']?>" selected="selected"><?= $doctor_data['d_city']?>, <?= $doctor_data['zipcode'] ?></option>
                      <?php if(count($city)):?>
                    <?php foreach ($city as $cityy):?>

                        <option value="<?= $cityy['city_id'] ?>"><?= $cityy['d_city'] ?>, <?= $cityy['zipcode'] ?></option>
                    <?php endforeach;?>
                <?php endif;?>
              </select>
              
              </div>
              <div class="clearfix"></div>
            </div>

            <div class="edit_profile_div">
              <label class="edit_profile_label">Sex</label>
              <div class="edit_profile_text col-md-4">
                <select class="edit_profile_textboxfull" name="d_gender">
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                </select>
              </div>
              <div class="clearfix"></div>
            </div>

            <div class="edit_profile_div">
              <label class="edit_profile_label">Date of Birth</label>
              <div class="edit_profile_text col-md-6">
              <input type="" id= "datepicker" class="edit_profile_textboxfull" name="d_dob" placeholder="Date of Birth" value=<?= $doctor_data['d_dob']?>  readOnly >

               
              </div>
              
              
              <div class="clearfix"></div>
            </div>


            <div class="">
             <?php echo form_submit(['name'=>'submit','class'=>'bookappointment_login_button','value'=>'Update'])?>
             
            </div>


          </div>
        </div>
         <?php echo form_close(); ?>
      </div>
    </div>
  
    <!-- *************** Main Part Close *************** -->

 <?php include_once ('footer.php'); ?>
<script>  
$(document).ready(function() {
    $("#datepicker").datepicker({ dateFormat: 'dd-mm-yy' });
  });
</script>
   
    </script>
  </body>
</html>
