<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Vertilib</title>
    <?= link_tag('assets/css/bootstrap.min.css')?>
    <?= link_tag('assets/css/font-awesome.min.css')?>
    <?= link_tag('assets/css/style.css') ?>
    <?= link_tag('assets/css/fullcalendar.min.css');?>
    <script src="/vertilib/assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/moment.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/fullcalendar.min.js" charset="utf-8"></script>
    
  </head>
  <body>
    <!-- *************** Header Start *************** -->
    <div class="header_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo_text">
              <a href="<?php echo base_url() ?>" title="verterilib">Vertilib</a>
            </div>
          </div>
         
<?php if(!empty($doctor_data)){?>

          <div class="col-md-8 text-right">
            <div class="profile_button">
              <div class="btn-group">
                <button type="button" class="btn btn-danger"> <i class="fa fa-user-md" aria-hidden="true"></i> <?= $doctor_data['d_firstname'] ?></button>
                <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="sr-only"><i class="fa fa-bars" aria-hidden="true"></i></span>
                </button>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="<?php echo base_url()?>Account"> Dashboard </a>
                  <a class="dropdown-item" href="<?php echo base_url()?>Account/edit_profile"> Profile </a>
                  <a class="dropdown-item" href="<?php echo base_url()?>Account/appoint"> Appointments </a>
                   <a class="dropdown-item" href="<?php echo base_url() ?>Time_slot"> Addtimeslot </a>
                  <a class="dropdown-item" href="<?php echo base_url()?>Account/password"> Change Password </a>
                  <a class="dropdown-item" href="<?php echo base_url()?>Account/logout"> Sign Out </a>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <?php } ?>


      </div>
    </div>
    <!-- *************** Header Close *************** -->


    <!-- *************** Main Part Start *************** -->
    <div class="singin_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <label class="profile_lable">Dashboard</label>
          </div>
        </div>

        <br><br>


        <div class="row">
          <div class="col-md-4">
            <div class="dashboard-text">
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,
              </p>

              <div class="btn-group" id="toggle_event_editing">
                <button type="button" class="btn btn-info locked_active">Available</button>
                <button type="button" class="btn btn-default unlocked_inactive">Not Available </button>
              </div>
             <?php if(!empty($doctor_data)){?>
            
              <div class="book_docter_details">
                <div class="book_docter_details_lable">Profile</div>

                <div class="doctor_details">
                  <div class="">
                    <div class="col-md-4">
                      <div class="book_doctor_img">
                        <img src=<?= $doctor_data['d_image'] ?> alt="" width="80"/>
                      </div>
                    </div>
                    <div class="col-md-8">
                      <label class="book_doctor_name">Dr. <?= $doctor_data['d_firstname'] ?> <?= $doctor_data['d_lastname'] ?></label>
                      <div class="book_doctor_reating">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                      </div>
                      <div class="book_doctor_text">
                       <p><?= $doctor_data['d_city'] ?> - <?= $doctor_data['zipcode'] ?></p>
                      </div>
                    </div>
                    <div class="clear"></div>
                  </div>

                  <!-- <div class="book_docter_details_lable2 border_book_docter_details">Tuesday, December 27 - 11:45 AM</div> -->

                  <div class="book_docter_details_lable2 book_docter_details_border">
                    Email  <br>
                    <span><?= $doctor_data['d_email'] ?></span>
                  </div>

                  <div class="book_docter_details_lable2 book_docter_details_border">
                    Call  <br>
                    <span>+91 - <?= $doctor_data['d_phone'] ?></span>
                  </div>

                  <div class="book_docter_details_lable2 book_docter_details_border">
                    Specialty  <br>
                    <span><?= $doctor_data['d_specialty'] ?></span>
                  </div>

                  <div class="book_docter_details_lable2">
                    Clinic Name  <br>
                    <span><?= $doctor_data['d_clinic'] ?></span>
                  </div>

                </div>
              </div>
	  <?php } ?>
            </div>
          </div>
        
          <div class="col-md-8">
            <div class="dashboard-heading"> Appointment Calendar</div>
            <div id='calendar'></div>
          </div>
        </div>


      </div>
    </div>
    
    <!-- *************** Main Part Close *************** -->
 <?php include_once ('footer.php'); ?>

    <script>

    	$(document).ready(function() {

    		$('#calendar').fullCalendar({
    			header: {
    				left: 'prev,next today',
    				center: 'title',
    				right: 'month,basicWeek,basicDay'
    			},
    			defaultDate: '2017-01-01',
    			navLinks: true, // can click day/week names to navigate views
    			editable: true,
    			eventLimit: true, // allow "more" link when too many events
    			events: [
    		 <?php if(!empty($text)){ foreach($text as $texts){ $date=date_create($texts['date_from'].$texts["time_from"]); $date_new = date_format($date,"Y-m-d H:i:s");?>	
    			
    				{
    					title: 'your Created Appointment',
    					start: '<?php echo $date_new; ?>'
    				},
    				/*{
    					title: 'Long Event',
    					start: '2017-01-07',
    					end: '2017-01-10'
    				},
    				{
    					id: 999,
    					title: 'Repeating Event',
    					start: '2017-01-09T16:00:00'
    				},
    				{
    					id: 999,
    					title: 'Repeating Event',
    					start: '2017-01-16T16:00:00'
    				},
    				{
    					title: 'Conference',
    					start: '2017-01-11',
    					end: '2017-01-13'
    				},
    				{
    					title: 'Meeting',
    					start: '2017-01-12T10:30:00',
    					end: '2017-01-12T12:30:00'
    				},
    				{
    					title: 'Lunch',
    					start: '2017-01-12T12:00:00'
    				},
    				{
    					title: 'Meeting',
    					start: '2017-01-12T14:30:00'
    				},
    				{
    					title: 'Happy Hour',
    					start: '2017-01-12T17:30:00'
    				},
    				{
    					title: 'Dinner',
    					start: '2017-01-12T20:00:00'
    				},
    				{
    					title: 'Birthday Party',
    					start: '2017-01-13T07:00:00'
    				},*/
    				<?php }} ?>
    				{
    					title: 'Click for Google',
    					url: 'http://google.com/',
    					start: '2017-01-28'
    				}
    			]
    			
    		});

        $('#toggle_event_editing button').click(function(){
        	/* reverse locking status */
        	$('#toggle_event_editing button').eq(0).toggleClass('locked_inactive locked_active btn-default btn-info');
        	$('#toggle_event_editing button').eq(1).toggleClass('unlocked_inactive unlocked_active btn-info btn-default');
        });

    	});

    </script>
  </body>
</html>
