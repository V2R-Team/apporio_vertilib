<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Vertilib</title>
     <?= link_tag('assets/css/bootstrap.min.css')?>
    <?= link_tag('assets/css/font-awesome.min.css')?>
    <?= link_tag('assets/css/style.css') ?>
    <?= link_tag('assets/css/bootstrap-select.min.css') ?>
    <script src="assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="assets/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="assets/js/bootstrap-select.min.js" charset="utf-8"></script>
   
  </head>
  <body>
    <!-- *************** Header Start *************** -->
    <div class="header_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo_text">
              <a href="index.php" title="Vertilib">Vertilib</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- *************** Header Close *************** -->

    <!-- Book Appointment Steps Start -->
    <div class="bookappointment_steps_bg">
      <div class="bookappointment_step1">
        <div class="bookappointment_steps_one flow-next active"> <div class="full">Patient Info</div> </div>
      </div>
      <div class="bookappointment_step2">
        <div class="bookappointment_steps_one flow-next active"> <div class="full">Appt Info</div> </div>
      </div>
      <div class="bookappointment_step3">
        <div class="bookappointment_steps_one flow-next active"> <div class="full">Verify</div> </div>
      </div>
      <div class="bookappointment_step4">
        <div class="bookappointment_steps_one flow-next not-active"> <div class="full">Finished!</div> </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <!-- Book Appointment Steps Start -->

    <!-- *************** Main Part Start *************** -->
    <div class="bookappointment_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="bookappointment_heading">Book Your Appointment</div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-8">
            <div class="bookappointment_div">
              <div class="bookappointment_subheading">
                Verify your phone number
                <span>We'll send a PIN to this number right away</span>
              </div>

              <br>

              <div class="">
                <div class="col-md-4 padding_none">
                  <input type="text" name="name" value="" class="bookappointment_login_textbox" placeholder="e.g. (866) 962-3621">
                </div>
                <div class="col-md-4">
                  <div class="bookappointment_info">
                    For Non-US numbers, please enter your international code starting with a '+' sign. e.g. +1-866-962-3621
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>

              <div class="">
                <div class="col-md-2 padding_none">
                  <input type="button" name="name" class="bookappointment_login_button" value="Call Me">
                </div>
                <div class="col-md-2 padding_none">
                  <input type="button" name="name" class="bookappointment_login_button" value="Text Me">
                </div>
              </div>

              <br><br>


            </div>
          </div>

          <div class="col-md-4">
            <div class="book_docter_details">
              <div class="book_docter_details_lable">Doctor</div>

              <div class="doctor_details">
                <div class="">
                  <div class="col-md-4">
                    <div class="book_doctor_img">
                      <img src="images/2741circle_medium.png" alt="" width="80"/>
                    </div>
                  </div>
                  <div class="col-md-8">
                    <label class="book_doctor_name">Dr. Mahyar Eidgah MD</label>
                    <div class="book_doctor_reating">
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star-o" aria-hidden="true"></i>
                    </div>
                    <div class="book_doctor_text">
                      <p>310 East 14th St 3rd Floor New York, NY 10003</p>
                    </div>
                  </div>
                  <div class="clear"></div>
                </div>

                <div class="book_docter_details_lable2 border_book_docter_details">Tuesday, December 27 - 11:45 AM</div>

                <div class="book_docter_details_lable2">
                  Patient <br>
                  <span>TBD</span>
                </div>

                <div class="book_docter_details_lable2">
                  Reason for Visit <br>
                  <span>Illness</span>
                </div>

              </div>
            </div>

            <span class="secure_booking"> <i class="fa fa-lock" aria-hidden="true"></i> Secure Booking </span>
          </div>



        </div>
      </div>
    </div>
    <!-- *************** Main Part Close *************** -->

    <!-- *************** Footer Start *************** -->
    <div class="footer_one_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-7">
            <div class="footer_one_text">
              Need help booking online? &nbsp; (123) 456-7890
            </div>
          </div>
          <div class="col-md-5">
            <div class="footer_one_text2">
              <a href="#">Service@Vertilib.com</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="footer_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-2">
            <div class="footer_list">
              <span>Vertilib</span>

              <ul class="footer_menu">
                <li> <a href="#" title="About">About</a> </li>
                <li> <a href="#" title="Press">Press</a> </li>
                <li> <a href="#" title="Careers">Careers</a> </li>
                <li> <a href="#" title="Contact">Contact</a> </li>
                <li> <a href="#" title="Answers">Answers</a> </li>
                <li> <a href="#" title="FAQ">FAQ</a> </li>
                <li> <a href="#" title="Blog">Blog</a> </li>
                <li> <a href="#" title="Doctor Blog">Doctor Blog</a> </li>
              </ul>
            </div>
          </div>

          <div class="col-md-2">
            <div class="footer_list">
              <span>Search By</span>

              <ul class="footer_menu">
                <li> <a href="#" title="Doctor Name">Doctor Name</a> </li>
                <li> <a href="#" title="Practice Name">Practice Name</a> </li>
                <li> <a href="#" title="Specialty">Specialty</a> </li>
                <li> <a href="#" title="Procedure">Procedure</a> </li>
                <li> <a href="#" title="Language">Language</a> </li>
                <li> <a href="#" title="Location">Location</a> </li>
                <li> <a href="#" title="Hospital">Hospital</a> </li>
                <li> <a href="#" title="Insurance">Insurance</a> </li>
              </ul>
            </div>
          </div>


          <div class="col-md-2">
            <div class="footer_list">
              <span>Cities</span>

              <ul class="footer_menu">
                <li> <a href="#" title="Chicago">Chicago</a> </li>
                <li> <a href="#" title="Houston">Houston</a> </li>
                <li> <a href="#" title="New York">New York</a> </li>
                <li> <a href="#" title="Philadelphia">Philadelphia</a> </li>
                <li> <a href="#" title="Phoenix">Phoenix</a> </li>
                <li> <a href="#" title="San Antonio">San Antonio</a> </li>
                <li> <a href="#" title="Washington DC">Washington DC</a> </li>
              </ul>
            </div>
          </div>

          <div class="col-md-2">
            <div class="footer_list">
              <span>Specialties</span>

              <ul class="footer_menu">
                <li> <a href="#" title="Chiropractors">Chiropractors</a> </li>
                <li> <a href="#" title="Dentists">Dentists</a> </li>
                <li> <a href="#" title="Dermatologists">Dermatologists</a> </li>
                <li> <a href="#" title="Eye Doctors">Eye Doctors</a> </li>
                <li> <a href="#" title="Gynecologists">Gynecologists</a> </li>
                <li> <a href="#" title="Primary Care Doctors">Primary Care Doctors</a> </li>
                <li> <a href="#" title="Psychiatrists">Psychiatrists</a> </li>
              </ul>
            </div>
          </div>

          <div class="col-md-2">
            <div class="footer_list">
              <span>Are You a Top Doctor?</span>

              <ul class="footer_menu">
                <li> <a href="#" title="Join Zocdoc today!">Join Zocdoc today!</a> </li>
              </ul>
            </div>

            <br>

            <div class="footer_list">
              <span>Vertilib for Employers</span>

              <ul class="footer_menu">
                <li> <a href="#" title="Learn More">Learn More</a> </li>
              </ul>
            </div>

            <br>

            <div class="footer_list">
              <span>Zocdoc for Health Systems</span>

              <ul class="footer_menu">
                <li> <a href="#" title="Learn More">Learn More</a> </li>
              </ul>
            </div>
          </div>

          <div class="col-md-2">
            <div class="footer_list">
              <span>Follow Vertilib</span>

              <ul class="footer_menu">
                <li> <a href="#" title="Facebook"> <i class="fa fa-facebook" aria-hidden="true"></i>  Facebook</a> </li>
                <li> <a href="#" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter </a> </li>
                <li> <a href="#" title="Google+"><i class="fa fa-google-plus" aria-hidden="true"></i> Google+ </a> </li>
                <li> <a href="#" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i> Linkedin </a> </li>
              </ul>
            </div>
          </div>

        </div>


        <div class="row">
          <div class="col-md-12">
            <div class="footer_copy_text">
              Our <a href="#" title="Privacy Policy</">Privacy Policy</a> and <a href="#" title="Terms of Use">Terms of Use</a> &copy;2017 Zocdoc, Inc.
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- *************** Footer Close *************** -->

    <!-- Footer Js -->
   
  </body>
</html>
