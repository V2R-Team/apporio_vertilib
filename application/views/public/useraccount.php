<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Vertilib</title>
    <?= link_tag('assets/css/bootstrap.min.css')?>
    <?= link_tag('assets/css/font-awesome.min.css')?>
    <?= link_tag('assets/css/style.css') ?>
    <?= link_tag('assets/css/bootstrap-reset.css') ?>
    <?= link_tag('assets/css/stylesheet.css') ?>
    <?= link_tag('assets/css/fullcalendar.min.css');?>
    <script src="/vertilib/assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/moment.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/fullcalendar.min.js" charset="utf-8"></script>
  </head>
  <body>
<!-- *************** Header Start *************** -->

    <div class="header_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo_text">
              <a href="http://apporio.org/vertilib/" title="Veterilib ">Veterilib </a>
            </div>
          </div>
<?php if(!empty($user_data)){?>

          <div class="col-md-8 text-right">
            <div class="profile_button">
              <div class="btn-group">
                <button type="button" class="btn btn-danger"> <i class="fa fa-user" aria-hidden="true"></i><?= $user_data[0]['p_firstname'] ?></button>
                <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="sr-only"><i class="fa fa-bars" aria-hidden="true"></i></span>
                </button>
                <div class="dropdown-menu">
               
                   <a class="dropdown-item" href="<?php echo base_url();?>Useraccount"> Medical Team </a>
                  <a class="dropdown-item" href="<?php echo base_url();?>Useraccount/user_account"> Past Appointments </a>
                  <a class="dropdown-item" href="<?php echo base_url();?>Useraccount/settings"> Settings </a>
                  <a class="dropdown-item" href="<?php echo base_url();?>Useraccount/logout"> Sign Out </a>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        

      </div>
    </div>
    <!-- *************** Header Close *************** -->


    <!-- *************** Main Part Start *************** -->
    <div class="singin_bg1" style="margin:5px 0px 20px 0px;">
      <div class="container">
        

<div class="profile_tab"> 
                     <ul>
                    	<li style="margin-left:0px;"><a href="<?php echo base_url();?>Useraccount" class="active"><h3 style="padding-left:0px; border:none;" class="title">Medical Team</h3></a></li>
                        <li><a href="<?php echo base_url();?>Useraccount/user_account"><h3 class="title">Past Appointments and Reviews</h3></a></li>
                        <li><a href="<?php echo base_url();?>Useraccount/settings"><h3 class="title">Settings</h3></a></li>
                    </ul>
                </div>
                <br>
                <div class="row" >
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                <div class="col-md-9">
                                  <h1><small>Welcome, <?= $user_data[0]['p_firstname'] ?>!</small></h1>
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                  <a href="<?php echo base_url();?>" class="btn btn-purple m-b-5" role="button" style="margin:30px 0px !important;">Find Doctor</a>
                                  
                                </div>
                                <div class="col-md-3">
                                 <img src="http://apporio.org/vertilib/assets/images/Logo1.jpg" title="Logo" alt="Logo"/>
                                </div>
                                </div>
                                
                                <div class="row">
                    <div class="col-lg-6">
                        <div class="portlet">
                            <div class="portlet-heading bg-purple">
                                <h3 class="portlet-title">
                                    Book a Primary Care Physician
                                </h3>
                                <div class="portlet-widgets">
                                   
                                    <a href="#" data-toggle="remove"><i class="fa fa-close"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="bg-purple" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                   <div class="row">
                                   	<div class="col-md-4"><img src="http://apporio.org/vertilib/assets/images/dummyimg.jpg"/></div>
                                        <div class="col-md-8">Need a doctor? Book an appointment now and add a physician to your team.</div>
                                        <button style="margin:15px 0px 0px 10px;" type="button" class="btn btn-success m-b-5">Search</button>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
    
 <?php } ?>                
                     <div class="col-lg-6">
                        <div class="portlet">
                            <div class="portlet-heading bg-purple">
                                <h3 class="portlet-title">
                                    Book a Dentist
                                </h3>
                                <div class="portlet-widgets">
                                   
                                    <a href="#" data-toggle="remove"><i class="fa fa-close"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="bg-purple" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                   <div class="row">
                                   	<div class="col-md-4"><img src="http://apporio.org/vertilib/assets/images/dummyimg.jpg"/></div>
                                        <div class="col-md-8">Need a doctor? Book an appointment now and add a physician to your team.</div>
                                        <button style="margin:15px 0px 0px 10px;" type="button" class="btn btn-success m-b-5">Search</button>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-6">
                        <div class="portlet">
                            <div class="portlet-heading bg-purple">
                                <h3 class="portlet-title">
                                    Book a Dentist
                                </h3>
                                <div class="portlet-widgets">
                                   
                                    <a href="#" data-toggle="remove"><i class="fa fa-close"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="bg-purple" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                   <div class="row">
                                   	<div class="col-md-4"><img src="http://apporio.org/vertilib/assets/images/dummyimg.jpg"/></div>
                                        <div class="col-md-8">Need a doctor? Book an appointment now and add a physician to your team.</div>
                                        <button style="margin:15px 0px 0px 10px;" type="button" class="btn btn-success m-b-5">Search</button>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    

                    
                    
                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">No Upcoming Appointments</h3>
                            </div>
                            <div class="panel-body">
                                <p class="lead">This is a lead parapraph.</p> 
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p> 
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
        
      </div>
    </div>
    <!-- *************** Main Part Close *************** -->


 <?php include_once ('footer.php'); ?>

    <script>

    	$(document).ready(function() {

    		$('#calendar').fullCalendar({
    			header: {
    				left: 'prev,next today',
    				center: 'title',
    				right: 'month,basicWeek,basicDay'
    			},
    			defaultDate: '2016-12-12',
    			navLinks: true, // can click day/week names to navigate views
    			editable: true,
    			eventLimit: true, // allow "more" link when too many events
    			events: [
    				{
    					title: 'All Day Event',
    					start: '2016-12-01'
    				},
    				{
    					title: 'Long Event',
    					start: '2016-12-07',
    					end: '2016-12-10'
    				},
    				{
    					id: 999,
    					title: 'Repeating Event',
    					start: '2016-12-09T16:00:00'
    				},
    				{
    					id: 999,
    					title: 'Repeating Event',
    					start: '2016-12-16T16:00:00'
    				},
    				{
    					title: 'Conference',
    					start: '2016-12-11',
    					end: '2016-12-13'
    				},
    				{
    					title: 'Meeting',
    					start: '2016-12-12T10:30:00',
    					end: '2016-12-12T12:30:00'
    				},
    				{
    					title: 'Lunch',
    					start: '2016-12-12T12:00:00'
    				},
    				{
    					title: 'Meeting',
    					start: '2016-12-12T14:30:00'
    				},
    				{
    					title: 'Happy Hour',
    					start: '2016-12-12T17:30:00'
    				},
    				{
    					title: 'Dinner',
    					start: '2016-12-12T20:00:00'
    				},
    				{
    					title: 'Birthday Party',
    					start: '2016-12-13T07:00:00'
    				},
    				{
    					title: 'Click for Google',
    					url: 'http://google.com/',
    					start: '2016-12-28'
    				}
    			]
    		});

        $('#toggle_event_editing button').click(function(){
        	/* reverse locking status */
        	$('#toggle_event_editing button').eq(0).toggleClass('locked_inactive locked_active btn-default btn-info');
        	$('#toggle_event_editing button').eq(1).toggleClass('unlocked_inactive unlocked_active btn-info btn-default');
        });

    	});

    </script>
  </body>
</html>
