<?php include_once('header.php');?>


<!-- *************** Main Part Start *************** -->
<div class="main_part_bg">
    <div class="container">
     
 <?php if(count($text)):?>
      
        <?php
    $date = array(); 
   // $val = json_decode($text,true);
   // echo"<pre>";print_r($text);die();
   $doc = array();
   foreach($text as $k => $v)
 
   {
       $doc[$v['doctor_id']]['time_from'][]=$v['time_from'];
       $doc[$v['doctor_id']]['slot_id'][]=$v['slot_id'];
       $doc[$v['doctor_id']]['date_from'] = $v['date_from'];
       $doc[$v['doctor_id']]['d_clinic'] = $v['d_clinic'];
       $doc[$v['doctor_id']]['d_password'] = $v['d_password'];
       $doc[$v['doctor_id']]['d_firstname'] = $v['d_firstname'];
       $doc[$v['doctor_id']]['d_lastname'] = $v['d_lastname'];
       $doc[$v['doctor_id']]['specialty_id'] = $v['specialty_id'];
       $doc[$v['doctor_id']]['d_phone'] = $v['d_phone'];
       $doc[$v['doctor_id']]['city_id'] = $v['city_id'];
       $doc[$v['doctor_id']]['d_gender'] = $v['d_gender'];
       $doc[$v['doctor_id']]['d_image'] = $v['d_image'];
       $doc[$v['doctor_id']]['d_specialty'] = $v['d_specialty'];
       $final_date =  $doc[$v['doctor_id']]['date_from'];
   } ?>
   
    <div class="row">
            <div class="col-md-10">

                <div class="date_filter">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="" >
                            
                            <div class="search_date_text">
<?php $dt = strtotime($final_date); $day = date("D", $dt); print strtoupper($day); ?> <br> <?= $final_date ?>
                            </div>

                           
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
    </div>
 <?php 
 //echo"<pre>"; print_r($doc);die();
    
  foreach ($doc as $keys => $value):
 //if($value->{'doctor_id'} ==$doc[]] )    

  ?>
        <div class="row">
            <div class="col-md-10">

                <!--<div class="date_filter">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="" style="float:right;">
                            <a href="#" class="search_date_icon"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a>
                            <div class="search_date_text">
                                Mon <br> <?= $value['date_from'] ?>
                            </div>

                            <a href="#" class="search_date_icon2"> <i class="fa fa-angle-right" aria-hidden="true"></i> </a>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>-->


                <div class="main_part">
                    <div class="main_search_part">
                        <div class="col-md-2">
                            <img src=<?= $value['d_image'] ?> alt="doc-img" class="img-circle "width="108" />
                        </div>
                        <div class="col-md-5">
                            <div class="main_search_name"> <a href="#"> Dr. <?= $value['d_firstname'] ?> <?= $value['d_lastname'] ?></a>
                                <span>Veterinary Doctor</span>
                            </div>

                            <div class="main_search_reating">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                            </div>

                            <div class="main_search_text">
                                <p><?= $value['d_specialty'] ?></p>
                            </div>

                            <div class="main_search_text">
                                <p><?= $value['d_clinic'] ?></p>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <ul class="search_time_list">
                                <?php $timeee = 1; foreach($value['time_from'] as $k=>$time) { if($timeee <= 5){ if($this->session->userdata('patient_id')){ ?>
                                 
                                <li> <a href="/vertilib/Booking/booking_login?slot_id=<?= $value['slot_id'][$k] ?>&book=direct-book "><?= $time ?></a> </li>
                                <?php } if(!$this->session->userdata('patient_id')) { ?>
                                <li> <a href="/vertilib/Booking/booking_time?slot_id=<?= $value['slot_id'][$k] ?>"><?= $time ?></a> </li>
                                <?php }} if($timeee > 5){ if($this->session->userdata('patient_id')){ ?>
                                 <div id="brar" style="display:none;" > <li> <a href="/vertilib/Booking/booking_login?slot_id=<?= $value['slot_id'][$k] ?>&book=direct-book "><?= $time ?></a> </li>
                                <?php } if(!$this->session->userdata('patient_id')) { ?>
                                 <div id="brar" style="display:none;" > <li> <a href="/vertilib/Booking/booking_time?slot_id=<?= $value['slot_id'][$k] ?>"><?= $time ?></a> </li></div>
                                
                                <?php }} $timeee++;  }  ?>
                                <span onclick="showfn();">more</span>
                            </ul>

                    

                        </div>
<script>
    function showfn()
    {
       // document.getElementById('brar').style.display = 'block';
        $('#brar').show();
    }
    </script>
                        <div class="clear"></div>
                    </div>



                </div>
            </div>


        </div>
        <?php endforeach;?>
 <?php endif;?>
    </div>
    <!-- *************** Main Part Close *************** -->
    <?php include_once('footer.php');?>
    <!-- *************** Footer Close *************** -->


    <script src="/vertilib/assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/bootstrap.min.js" charset="utf-8"></script>
</body>
</html>
