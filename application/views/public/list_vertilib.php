<!DOCTYPE html>
<?php include_once('header.php');?>
<?= link_tag('assets/css/select2.css') ?>
<style>

.image-preview-input {
    position: relative;
    overflow: hidden;
    margin: 0px;    
    color: #333;
    background-color: #fff;
    border-color: #ccc;    
}
.image-preview-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}

</style>


<!-- *************** Main Part Start *************** -->

<div class="singin_bg">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 style="margin-bottom:35px !important;">List your practice on Veterilib</h2>
        <!-- <a href="signin.php" class="singin_link">Already have one? Sign in.</a> --> 
      </div>
    </div>
    <?php echo form_open_multipart('Doctor/signup',['name'=>'Doc_signup']) ?>
    <div class="row">
      <div>
        <div class="">
          <div class="col-md-6">
            <div class="singup_lable">First Name</div>
            <?php echo form_input(['name'=>'d_firstname','class'=>'sign_textbox','placeholder'=>'First name','value'=>set_value('d_firstname'),"required"=>"required"])?> <?php echo "<div class='error'>".form_error('d_firstname')."</div>";?> </div>
          <div class="col-md-6">
            <div class="singup_lable">Last Name</div>
            <?php echo form_input(['name'=>'d_lastname','class'=>'sign_textbox','placeholder'=>'Last name','value'=>set_value('d_lastname'),"required"=>"required"])?> <?php echo "<div class='error'>".form_error('d_lastname')."</div>";?> </div>
          <div class="col-md-6">
            <div class="singup_lable">Your Direct Phone Number</div>
            <?php echo form_input(['name'=>'d_phone','class'=>'sign_textbox','placeholder'=>'Phone Number','value'=>set_value('d_phone'),"required"=>"required"])?> <?php echo "<div class='error'>".form_error('d_phone')."</div>";?> </div>
          <div class="col-md-6">
            <div class="singup_lable">Your Specialty</div>
            <select class="sign_textbox mygroup_selectbox2" name="specialty" required>
             <option value="">Select Your Specialty</option>
              <?php if(count($speciality)):?>
              <?php foreach ($speciality as $specialityy):?>
              <option value="<?= $specialityy['specialty_id'] ?>">
              <?= $specialityy['d_specialty'] ?>
              </option>
              <?php endforeach;?>
              <?php endif;?>
            </select>
            <?php echo "<div class='error'>".form_error('specialty')."</div>";?> </div>
          <div class="col-md-6">
            <div class="singup_lable">City Or Zipcode</div>
            <select class="sign_textbox mygroup_selectbox2" name="city" placeholder="Enter Your city" required>
              <option value="">Select Your City</option>
              <?php if(count($city)):?>
              <?php foreach ($city as $cityy):?>
              <option value="<?= $cityy['city_id'] ?>">
              <?= $cityy['d_city'] ?>
              ,
              <?= $cityy['zipcode'] ?>
              </option>
              <?php endforeach;?>
              <?php endif;?>
            </select>
            <?php echo "<div class='error'>".form_error('city')."</div>";?> </div>
          <div class="col-md-6">
            <div class="singup_lable">Your Email</div>
            <?php echo form_input(['name'=>'d_email','class'=>'sign_textbox','placeholder'=>'Your Email','value'=>set_value('d_email'),"required"=>"required"])?> <?php echo "<div class='error'>".form_error('d_email')."</div>";?> </div>
          <div class="col-md-6">
            <div class="singup_lable">Password</div>
            <?php echo form_password(['name'=>'d_password','class'=>'sign_textbox','placeholder'=>'Your Password',"required"=>"required"])?> <?php echo "<div class='error'>".form_error('d_password')."</div>";?> </div>
          <div class="col-md-6">
            <div class="singup_lable">Clinic Name</div>
            <?php echo form_input(['name'=>'d_clinic','class'=>'sign_textbox','placeholder'=>'Clinic Address','value'=>set_value('d_clinic'),"required"=>"required"])?> <?php echo "<div class='error'>".form_error('')."</div>";?> </div>
          
          
          <div class="col-md-6">
            <div class="singup_lable">Animal</div>
            <div class="">
              <select class="select2" multiple data-placeholder="Select a Animal..." name="animal[]" required>
                <option value="#">&nbsp;</option>
               <?php if(count($animal)):?>
              <?php foreach ($animal as $animal):?>
              <option value="<?= $animal['animal_name'] ?>">
              <?= $animal['animal_name'] ?>
              </option>
              <?php endforeach;?>
              <?php endif;?>
              </select>
            </div>
          </div>
            <div class="col-md-6">
            <div class="singup_lable">Image</div>
            <div class="">
              <?php echo form_upload(['name'=>'d_image','class'=>'sign_textbox',"required"=>"required"])?>  
            </div>
            <?php if(isset($error)) echo $error; ?>
          </div>
          
          
           
          
          <!-- <div class="">
                <div class="funkyradio padding_none">
                    <div class="funkyradio-primary col-md-6 padding_none">
                        <input type="radio" name="radio" id="radio1" checked/>
                        <label for="radio1">Male</label>
                    </div>
                    <div class="funkyradio-primary col-md-6 padding_none">
                        <input type="radio" name="radio" id="radio2"/>
                        <label for="radio2">Female</label>
                    </div>
                    <div class="clear"></div>
                </div>
              </div> --> 
          
          <!-- <div class="">
                <div class="terms_lable_signup">
                    <label>
                      <span class="terms_lable_checkbox"> <input type="checkbox" name="terms" id="terms" value=""> </span> Please read and accept the <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.
                      <div class="clear"></div>
                    </label>
                </div>
              </div> -->
          <div class="clear"></div>
          <div class="col-md-6" style="margin-top:25px;"> <?php echo form_submit(['name'=>'submit','class'=>'sign_button','value'=>'Get Started'])?> </div>
          <br>
          <br>
          <br>
        </div>
      </div>
      <?php echo form_close(); ?> </div>
  </div>
</div>
<!-- *************** Main Part Close *************** -->

<?php include_once('footer.php');?>

<!-- Footer Js --> 
<script src="/vertilib/assets/js/jquery.min.js" charset="utf-8"></script> 
<script src="/vertilib/assets/js/bootstrap.min.js" charset="utf-8"></script> 
<script src="/vertilib/assets/js/jquery.tagsinput.min.js" charset="utf-8"></script> 
<script src="/vertilib/assets/js/toggles.min.js" charset="utf-8"></script> 
<script src="/vertilib/assets/js/bootstrap-timepicker.min.js" charset="utf-8"></script> 
<script src="/vertilib/assets/js/bootstrap-datepicker.js" charset="utf-8"></script> 
<script src="/vertilib/assets/js/bootstrap-colorpicker.js" charset="utf-8"></script> 
<script src="/vertilib/assets/js/jquery.multi-select.js" charset="utf-8"></script> 
<script src="/vertilib/assets/js/jquery.quicksearch.js" charset="utf-8"></script> 
<script src="/vertilib/assets/js/spinner.min.js" charset="utf-8"></script> 
<script src="/vertilib/assets/js/select2.min.js" charset="utf-8"></script> 
<script type="text/javascript">
$(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
           $('.image-preview').popover('show');
        }, 
         function () {
           $('.image-preview').popover('hide');
        }
    );    
});

$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Browse"); 
    }); 
    // Create the preview image
    $(".image-preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }        
        reader.readAsDataURL(file);
    });  
});
</script>
<script>
            jQuery(document).ready(function() {
                    
                // Tags Input
                jQuery('#tags').tagsInput({width:'auto'});

                // Form Toggles
                jQuery('.toggle').toggles({on: true});

                // Time Picker
                jQuery('#timepicker').timepicker({defaultTIme: false});
                jQuery('#timepicker2').timepicker({showMeridian: false});
                jQuery('#timepicker3').timepicker({minuteStep: 15});

                // Date Picker
                jQuery('#datepicker').datepicker();
                jQuery('#datepicker-inline').datepicker();
                jQuery('#datepicker-multiple').datepicker({
                    numberOfMonths: 3,
                    showButtonPanel: true
                });
                //colorpicker start

                $('.colorpicker-default').colorpicker({
                    format: 'hex'
                });
                $('.colorpicker-rgba').colorpicker();


                //multiselect start

                $('#my_multi_select1').multiSelect();
                $('#my_multi_select2').multiSelect({
                    selectableOptgroup: true
                });

                $('#my_multi_select3').multiSelect({
                    selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
                    selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
                    afterInit: function (ms) {
                        var that = this,
                            $selectableSearch = that.$selectableUl.prev(),
                            $selectionSearch = that.$selectionUl.prev(),
                            selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                            selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                        that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                            .on('keydown', function (e) {
                                if (e.which === 40) {
                                    that.$selectableUl.focus();
                                    return false;
                                }
                            });

                        that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                            .on('keydown', function (e) {
                                if (e.which == 40) {
                                    that.$selectionUl.focus();
                                    return false;
                                }
                            });
                    },
                    afterSelect: function () {
                        this.qs1.cache();
                        this.qs2.cache();
                    },
                    afterDeselect: function () {
                        this.qs1.cache();
                        this.qs2.cache();
                    }
                });

                //spinner start
                $('#spinner1').spinner();
                $('#spinner2').spinner({disabled: true});
                $('#spinner3').spinner({value:0, min: 0, max: 10});
                $('#spinner4').spinner({value:0, step: 5, min: 0, max: 200});
                //spinner end

                // Select2
                jQuery(".select2").select2({
                    width: '100%'
                });
            });
        </script>
</body></html>