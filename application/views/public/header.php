<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="icon" href="assets/images/Logo.jpg" type="image/gif" sizes="16x16">
    <title>Veterilib</title>
    <?= link_tag('assets/css/bootstrap.min.css')?>
    <?= link_tag('assets/css/font-awesome.min.css')?>
    <?= link_tag('assets/css/style.css') ?>
    <?= link_tag('assets/css/jquery-ui.css') ?>
 
<script src="assets/js/jquery.min.js" charset="utf-8"></script>
<script src="assets/js/bootstrap.min.js" charset="utf-8"></script>
<script src="assets/js/jquery1.min.js" charset="utf-8"></script>
<script src="assets/js/jquery-ui.min.js" charset="utf-8"></script>
<style>  
     
            .ui-menu {  
                list-style:none;  
                padding: 2px;  
                margin: 0;  
                display:block;  
            }  
            .ui-menu .ui-menu {  
                margin-top: -3px;  
            }  
            .ui-menu .ui-menu-item {  
                margin:0;  
                padding: 0;  
                zoom: 1;  
                float: left;  
                clear: left;  
                width: 100%;  
                font-size:80%;  
            }  
            .ui-menu .ui-menu-item a {  
                text-decoration:none;  
                display:block;  
                padding:.2em .4em;  
                line-height:1.5;  
                zoom:1;  
            }  
            .ui-menu .ui-menu-item a.ui-state-hover,  
            .ui-menu .ui-menu-item a.ui-state-active {  
                font-weight: normal;  
                margin: -1px;  
            }  
        </style>  
  </head>
 
  <body>
    <!-- *************** Header Start *************** -->
    <div class="header_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo_text">
                <?= anchor('vertilib','Veterilib',['class'=>''])?>
              
            </div>
          </div>

          <div class="col-md-8 text-right">
            <div class="header_button_commen">
              <a href="<?php echo base_url() ?>Doctor" class="header_button_commen_one">List your practice on Veterilib</a>
              <a href="<?php echo base_url() ?>Signin">Sign In/Join</a>
             
            </div>
          </div>
        </div>
<?php echo form_open('Lookup',['name'=>'search_data']) ?>
        <div class="row">
            <div class="col-md-8 first_header_textbox">
<?php echo form_input(['name'=>'d_firstname','class'=>'header_textbox2','placeholder'=>'Clinic Name','id'=>'d_firstname'])?>
            </div>
            <div class="col-md-4 padding_none header_textbox_one">
  <input name="city" id="city" type="text" class="header_textbox2" placeholder="City OR Pincode">           

  
<button type="submit" name="button" class="header_search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
            </div>
        </div>
<?php echo form_close(); ?>

      </div>
    </div>
<script type="text/javascript">  
var j = jQuery.noConflict();
         j(this).ready( function() {  
            j("#city").autocomplete({  
  
           //search_data = $("#search_from").val() ;
            minLength: 1,  
                source:   
                function(req, add){  
                    $.ajax({  
                        url: "<?php echo base_url(); ?>Lookup/citydata",  
                        dataType: 'json',  
                        type: 'POST',    
                        data: 'city='+$("#city").val(),  
                        success:      
                        function(data){  
                            if(data.response == "true"){ 
                                add(data.message);  
                                console.log(data);
                            }  
                        },  
                    });  
                },  
                     
            });  
        });  
</script>
<script type="text/javascript">  
var j = jQuery.noConflict();
         j(this).ready( function() {  
            j("#d_firstname").autocomplete({  
  
           //search_data = $("#search_from").val() ;
            minLength: 1,  
                source:   
                function(req, add){  
                    $.ajax({  
                        url: "<?php echo base_url(); ?>Lookup/doctor_data",  
                        dataType: 'json',  
                        type: 'POST',    
                        data: 'd_firstname='+$("#d_firstname").val(),  
                        success:      
                        function(data){  
                            if(data.response == "true"){ 
                                add(data.message);  
                                console.log(data);
                            }  
                        },  
                    });  
                },  
                     
            });  
        });  
</script>