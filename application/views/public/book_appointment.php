<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="icon" href="assets/images/Logo.jpg" type="image/gif" sizes="16x16">
    <title>Veterilib</title>
     <?= link_tag('assets/css/bootstrap.min.css')?>
    <?= link_tag('assets/css/font-awesome.min.css')?>
    <?= link_tag('assets/css/style.css') ?>
    <?= link_tag('assets/css/bootstrap-select.min.css') ?>
     <script src="/vertilib/assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/bootstrap.min.js" charset="utf-8"></script>
  </head>
  <body>
    <!-- *************** Header Start *************** -->
    <div class="header_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo_text">
              <a href="<?php echo base_url() ?>" title="Veterilib">Veterilib</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- *************** Header Close *************** -->

    <!-- Book Appointment Steps Start -->
    <div class="bookappointment_steps_bg">
      <div class="bookappointment_step1">
        <div class="bookappointment_steps_one flow-next active"> <div class="full">Patient Info</div> </div>
      </div>
      <div class="bookappointment_step2">
        <div class="bookappointment_steps_one flow-next not-active"> <div class="full">Appt Info</div> </div>
      </div>
      <div class="bookappointment_step3">
        <div class="bookappointment_steps_one flow-next not-active"> <div class="full">Verify</div> </div>
      </div>
      <div class="bookappointment_step4">
        <div class="bookappointment_steps_one flow-next not-active"> <div class="full">Finished!</div> </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <!-- Book Appointment Steps Start -->

    <!-- *************** Main Part Start *************** -->
    <div class="bookappointment_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="bookappointment_heading">Book Your Appointment</div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-8">
            <div class="bookappointment_text">
              <b>Have you used Zocdoc before?</b> <br>
              We'll use the information from your last visit
            </div>

            <div class="bookappointment_type">
              <div class="">
                  <?php  if(!empty($form1)) { ?>
                <label> <input type="radio" name="user_type" id="register_user" onclick="check_type(1)" value="1"> &nbsp; I'm new to Veterilib. </label>
             <script>$( document ).ready(function() {$("#bookappointment_register").slideDown('200');
        $("#bookappointment_login").slideUp('200'); });</script>
             <?php } else{ ?>
<label> <input type="radio" name="user_type" id="register_user" onclick="check_type(1)" value="1"> &nbsp; I'm new to Vertilib. </label>
             <?php } ?>
              </div>
              <div class="">
              <?php  if(!empty($form2)) { ?>
                  <label> <input type="radio" checked="checked"  name="user_type" id="login_user" onclick="check_type(2)" value="2"> &nbsp; I've used Veterilib before. </label>
                  <script>$( document ).ready(function() {$("#bookappointment_login").slideDown('200');
        $("#bookappointment_register").slideUp('200'); });</script>
                  <?php } else{ ?>
                  <label> <input type="radio" name="user_type" id="login_user" onclick="check_type(2)" value="2"> &nbsp; I've used Vertilib before. </label>
                  <?php } ?>
              </div>
            </div>

<?php echo form_open('Booking/booking_login',['name'=>'singin']) ?>
    
            <div class="bookappointment_login" id="bookappointment_login">
              <div class="bookappointment_text">
                <b>Welcome back!</b> <br>
                Please sign in to your account
              </div>

              <div class="bookappointment_login_div">
                <div class="col-md-5 padding_none">
              <?php  if (isset($error)){
    echo "<div class='error' style=color:red;>$error</div>";
   
}?> 
                  <label>Email</label>
                  <?php if(isset($booking_data[0]['slot_id'])){ ?>
                  <input type="hidden" name="slot_id" value="<?php echo $booking_data[0]['slot_id']; ?>">
                  <?php } ?>
                  <?php if(isset($_GET['slot_id'])){ ?>
                  <input type="hidden" name="slot_id" value="<?= $_GET['slot_id']; ?>">
                  <?php } ?>
                  <input type="hidden" id="user_type" name="radio_val" value="2">
                  <input type="text" class="bookappointment_login_textbox" name="p_email" value="" placeholder="Email Address">
                  <?php echo "<div class='error'>".form_error('p_email')."</div>";?>
                  <br><br>
                  <label>Password</label>
                  <input type="password" class="bookappointment_login_textbox" name="p_password" value="" placeholder="Password">
                  <a href="#">Forgot your password?</a>

                  <br><br>
                  <?php echo form_submit(['name'=>'submit','class'=>'bookappointment_login_button','id'=>'submit','value'=>'Sign In'])?>
              
                 
                </div>
                <div class="clear"></div>
              </div>
            </div>
<?php echo form_close(); ?>
<?php echo form_open('Bookingsignup/booking_signup',['name'=>'signup']) ?>
            <div class="bookappointment_login" id="bookappointment_register">
              <div class="bookappointment_text">
                <b>Create an account</b> <br>
                This will help you manage your appointments
              </div>

              <div class="bookappointment_login_div">
                <div class="col-md-8 padding_none">
                    <?php if(isset($booking_data[0]['slot_id'])){ ?>
                  <input type="hidden" name="slot_id" value="<?php echo $booking_data[0]['slot_id']; ?>">
                  <?php } ?>
                  <?php if(isset($_GET['slot_id'])){ ?>
                  <input type="hidden" name="slot_id" value="<?= $_GET['slot_id']; ?>">
                  <?php } ?>
       <input type="hidden" id="user_type" name="radio_val" value="1">
                  <label>Email</label>
 <?php echo form_input(['name'=>'p_email','class'=>'bookappointment_login_textbox','placeholder'=>'Email Adddress','value'=>set_value('p_email')])?>
     <?php echo "<div class='error'>".form_error('p_email')."</div>";?>
                  <br><br>

                  <label>Confirm Your Email</label>
<?php echo form_input(['name'=>'cp_email','class'=>'bookappointment_login_textbox','placeholder'=>'Confirm your email'])?>
<?php echo "<div class='error'>".form_error('cp_email')."</div>";?>
                  <br>

                  <br>
                  <label>Create a Password</label>
<?php echo form_password(['name'=>'p_password','class'=>'bookappointment_login_textbox','placeholder'=>'Enter Password'])?>
<?php echo "<div class='error'>".form_error('p_password')."</div>";?>
                  <br><br>

                  <label>Name</label>
                  <div class="">
                    <div class="col-md-6 padding_none_left">
<?php echo form_input(['name'=>'p_firstname','class'=>'bookappointment_login_textbox','placeholder'=>'First Name','value'=>set_value('p_firstname')])?>
                    </div>
                    <div class="col-md-6 padding_none">
<?php echo form_input(['name'=>'p_lastname','class'=>'bookappointment_login_textbox','placeholder'=>'Last Name','value'=>set_value('p_lastname')])?>
                    </div>
                    <div class="clear"></div>
                  </div>

                  <br><br>

                  <label>Date of Birth</label>
                  <div class="">
                    <div class="col-md-4 padding_none_left">
                      <select class="bookappointment_login_textbox" name="p_dob">
                        <option value="">Month</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                      </select>
                    </div>
                    <div class="col-md-4 padding_none_left">
                      <select class="bookappointment_login_textbox" name="p_dob1">
                        <option value="">Day</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                        <option value="31">31</option>
                      </select>
                    </div>

                    <div class="col-md-4 padding_none">
                      <select class="bookappointment_login_textbox" name="p_dob2">
                        <option value="2016">2016</option>
                        <option value="2015">2015</option>
                        <option value="2014">2014</option>
                        <option value="2013">2013</option>
                        <option value="2012">2012</option>
                        <option value="2011">2011</option>
                        <option value="2010">2010</option>
                        <option value="2009">2009</option>
                        <option value="2008">2008</option>
                        <option value="2007">2007</option>
                        <option value="2006">2006</option>
                        <option value="2005">2005</option>
                        <option value="2004">2004</option>
                        <option value="2003">2003</option>
                        <option value="2002">2002</option>
                        <option value="2001">2001</option>
                        <option value="2000">2000</option>
                        <option value="1999">1999</option>
                        <option value="1998">1998</option>
                        <option value="1997">1997</option>
                        <option value="1996">1996</option>
                        <option value="1995">1995</option>
                        <option value="1994">1994</option>
                        <option value="1993">1993</option>
                        <option value="1992">1992</option>
                        <option value="1991">1991</option>
                        <option value="1990">1990</option>
                        <option value="1989">1989</option>
                        <option value="1988">1988</option>
                        <option value="1987">1987</option>
                        <option value="1986">1986</option>
                        <option value="1985">1985</option>
                        <option value="1984">1984</option>
                        <option value="1983">1983</option>
                        <option value="1982">1982</option>
                        <option value="1981">1981</option>
                        <option value="1980">1980</option>
                        <option value="1979">1979</option>
                        <option value="1978">1978</option>
                        <option value="1977">1977</option>
                        <option value="1976">1976</option>
                        <option value="1975">1975</option>
                        <option value="1974">1974</option>
                        <option value="1973">1973</option>
                        <option value="1972">1972</option>
                        <option value="1971">1971</option>
                        <option value="1970">1970</option>
                        <option value="1969">1969</option>
                        <option value="1968">1968</option>
                        <option value="1967">1967</option>
                        <option value="1966">1966</option>
                        <option value="1965">1965</option>
                        <option value="1964">1964</option>
                        <option value="1963">1963</option>
                        <option value="1962">1962</option>
                        <option value="1961">1961</option>
                        <option value="1960">1960</option>
                        <option value="1959">1959</option>
                        <option value="1958">1958</option>
                        <option value="1957">1957</option>
                        <option value="1956">1956</option>
                        <option value="1955">1955</option>
                        <option value="1954">1954</option>
                        <option value="1953">1953</option>
                        <option value="1952">1952</option>
                        <option value="1951">1951</option>
                        <option value="1950">1950</option>
                        <option value="1949">1949</option>
                        <option value="1948">1948</option>
                        <option value="1947">1947</option>
                        <option value="1946">1946</option>
                        <option value="1945">1945</option>
                        <option value="1944">1944</option>
                        <option value="1943">1943</option>
                        <option value="1942">1942</option>
                        <option value="1941">1941</option>
                        <option value="1940">1940</option>
                        <option value="1939">1939</option>
                        <option value="1938">1938</option>
                        <option value="1937">1937</option>
                        <option value="1936">1936</option>
                        <option value="1935">1935</option>
                        <option value="1934">1934</option>
                        <option value="1933">1933</option>
                        <option value="1932">1932</option>
                        <option value="1931">1931</option>
                        <option value="1930">1930</option>
                        <option value="1929">1929</option>
                        <option value="1928">1928</option>
                        <option value="1927">1927</option>
                        <option value="1926">1926</option>
                        <option value="1925">1925</option>
                        <option value="1924">1924</option>
                        <option value="1923">1923</option>
                        <option value="1922">1922</option>
                        <option value="1921">1921</option>
                        <option value="1920">1920</option>
                        <option value="1919">1919</option>
                        <option value="1918">1918</option>
                        <option value="1917">1917</option>
                        <option value="1916">1916</option>
                        <option value="1915">1915</option>
                        <option value="1914">1914</option>
                        <option value="1913">1913</option>
                        <option value="1912">1912</option>
                        <option value="1911">1911</option>
                        <option value="1910">1910</option>
                        <option value="1909">1909</option>
                        <option value="1908">1908</option>
                        <option value="1907">1907</option>
                        <option value="1906">1906</option>
                        <option value="1905">1905</option>
                        <option value="1904">1904</option>
                        <option value="1903">1903</option>
                        <option value="1902">1902</option>
                        <option value="1901">1901</option>
                        <option value="1900">1900</option>
                        <option value="1899">1899</option>
                        <option value="1898">1898</option>
                        <option value="1897">1897</option>
                        <option value="1896">1896</option>
                        <option value="1895">1895</option>
                        <option value="1894">1894</option>
                        <option value="1893">1893</option>
                        <option value="1892">1892</option>
                        <option value="1891">1891</option>
                        <option value="1890">1890</option>
                        <option value="1889">1889</option>
                        <option value="1888">1888</option>
                        <option value="1887">1887</option>
                        <option value="1886">1886</option>
                      </select>
                    </div>
                    <div class="clear"></div>
                  </div>

                  <br><br>

                  <label>Sex</label>
                  <div class="">
                    <div class="col-md-5 padding_none_left">
                      <select class="bookappointment_login_textbox" name="d_gender">
                        <option value="">Select from list</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                      </select>
                    </div>
                  </div>

                  <br><br>

                  <div class="terms_lable terms_lable2">
                      <label>
                        <span class="terms_lable_checkbox2"> <input type="checkbox" name="terms" id="terms" value=""> </span> &nbsp; I have read and accept Vertilib <a href="#">Terms</a> of Use and <a href="#">Privacy Policy</a>.
                        <div class="clear"></div>
                      </label>
                  </div>

<?php echo form_submit(['name'=>'submit','class'=>'bookappointment_login_button','value'=>'Continue'])?>
                  
                </div>
              </div>
            </div>
          </div>
   <?php echo form_close(); ?>         
<?php if($booking_data != null){foreach($booking_data as $value){ ?>
          <div class="col-md-4">
            <div class="book_docter_details">
              <div class="book_docter_details_lable">Doctor</div>

              <div class="doctor_details">
                <div class="">
                  <div class="col-md-4">
                    <div class="book_doctor_img">
                     <img src=<?= $value['d_image'] ?> class="img-circle" alt="" width="80"/>
                    </div>
                  </div>
                  <div class="col-md-8">
                    <label class="book_doctor_name">Dr. <?= $value['d_firstname'] ?> <?= $value['d_lastname'] ?></label>
                    <div class="book_doctor_reating">
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star-o" aria-hidden="true"></i>
                    </div>
                    <div class="book_doctor_text">
                      <p><?= $value['d_city'] ?> - <?= $value['zipcode'] ?></p>
                    </div>
                  </div>
                  <div class="clear"></div>
                </div>

                <div class="book_docter_details_lable2 border_book_docter_details"><?= $value['date_from'] ?> - <?= $value['time_from'] ?></div>

                <div class="book_docter_details_lable2">
                  Specialty <br>
                  <span><?= $value['d_specialty'] ?></span>
                </div>

                <div class="book_docter_details_lable2">
                  Clinic Name <br>
                  <span><?= $value['d_clinic'] ?></span>
                </div>

              </div>
            </div>

            <span class="secure_booking"> <i class="fa fa-lock" aria-hidden="true"></i> Secure Booking </span>
          </div>
<?php }} ?>

        </div>
      </div>
    </div>
    <!-- *************** Main Part Close *************** -->

  
     <?php include_once('footer.php');?>
  

    <script type="text/javascript">
    function check_type(flag) {
      if(flag == 1) {
        $("#bookappointment_register").slideDown('200');
        $("#bookappointment_login").slideUp('200');
      } else if(flag == 2) {
      document.getElementById('user_type').value= 2;
        $("#bookappointment_login").slideDown('200');
        $("#bookappointment_register").slideUp('200');
      }
    }

    </script>

  </body>
</html>
