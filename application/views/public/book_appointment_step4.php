<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Veterilib</title>
    <?= link_tag('assets/css/bootstrap.min.css')?>
    <?= link_tag('assets/css/font-awesome.min.css')?>
    <?= link_tag('assets/css/style.css') ?>
    <?= link_tag('assets/css/bootstrap-select.min.css') ?>
    <script src="/vertilib/assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/bootstrap-select.min.js" charset="utf-8"></script>
  </head>
  <body>
    <!-- *************** Header Start *************** -->
    <div class="header_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo_text">
              <a href="<?php echo base_url() ?>" title="Veterilib">Veterilib</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- *************** Header Close *************** -->

    <!-- Book Appointment Steps Start -->
    <div class="bookappointment_steps_bg">
      <div class="bookappointment_step1">
        <div class="bookappointment_steps_one flow-next active"> <div class="full">Patient Info</div> </div>
      </div>
      <div class="bookappointment_step2">
        <div class="bookappointment_steps_one flow-next active"> <div class="full">Appt Info</div> </div>
      </div>
      <div class="bookappointment_step3">
        <div class="bookappointment_steps_one flow-next active"> <div class="full">Verify</div> </div>
      </div>
      <div class="bookappointment_step4">
        <div class="bookappointment_steps_one flow-next active"> <div class="full">Finished!</div> </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <!-- Book Appointment Steps Start -->

    <!-- *************** Main Part Start *************** -->
    <div class="bookappointment_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="bookappointment_heading">Book Your Appointment</div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-8">
            <div class="bookappointment_div">

              <div class="bookappointment_finished">
                Your Appointment Book Successfully...
                <span>
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                </span>
              </div>

              <br>

              <div class="">
              <a href="<?php echo base_url() ?>" class="btn btn-info btn-lg m-b-5" name="button" style="background-color:#a349a3;border-color:#a349a3;">Finish</a>
                
              </div>

              <br><br>


            </div>
          </div>
<?php if($booking_data != null){foreach($booking_data as $value){ ?>
          <div class="col-md-4">
            <div class="book_docter_details">
              <div class="book_docter_details_lable">Doctor</div>

              <div class="doctor_details">
                <div class="">
                  <div class="col-md-4">
                    <div class="book_doctor_img">
                 <img src=<?= $value['d_image'] ?> class="img-circle" alt="" width="80"/>
                    </div>
                  </div>
                  <div class="col-md-8">
                    <label class="book_doctor_name">Dr. <?= $value['d_firstname'] ?> <?= $value['d_lastname'] ?></label>
                    <div class="book_doctor_reating">
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star-o" aria-hidden="true"></i>
                    </div>
                    <div class="book_doctor_text">
                      <p><?= $value['d_city'] ?> - <?= $value['zipcode'] ?></p>
                    </div>
                  </div>
                  <div class="clear"></div>
                </div>

                <div class="book_docter_details_lable2 border_book_docter_details"><?= $value['date_from'] ?>-<?= $value['time_from'] ?></div>

                <div class="book_docter_details_lable2">
                  Specialty <br>
                  <span><?= $value['d_specialty'] ?></span>
                </div>

                <div class="book_docter_details_lable2">
                  Clinic Name <br>
                  <span><?= $value['d_clinic'] ?></span>
                </div>

              </div>
            </div>

            <span class="secure_booking"> <i class="fa fa-lock" aria-hidden="true"></i> Secure Booking </span>
          </div>
<?php }} ?>


        </div>
      </div>
    </div>
    <!-- *************** Main Part Close *************** -->
 
   <?php include_once('footer.php');?>
    
  </body>
</html>
