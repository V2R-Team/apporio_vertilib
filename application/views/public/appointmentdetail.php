<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Vertilib</title>
<?= link_tag('assets/css/bootstrap.min.css')?>
<?= link_tag('assets/css/font-awesome.min.css')?>
<?= link_tag('assets/css/style.css') ?>
<?= link_tag('assets/css/fullcalendar.min.css');?>
<script src="/vertilib/assets/js/jquery.min.js" charset="utf-8"></script>
<script src="/vertilib/assets/js/bootstrap.min.js" charset="utf-8"></script>
<script src="/vertilib/assets/js/moment.min.js" charset="utf-8"></script>
<script src="/vertilib/assets/js/fullcalendar.min.js" charset="utf-8"></script>
</head>
<body>
<!-- *************** Header Start *************** -->
<div class="header_bg">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="logo_text"> <a href="<?php echo base_url() ?>" title="Vertilib">Vertilib</a> </div>
      </div>
      <?php if(!empty($doctor_data)){?>
      
      <div class="col-md-8 text-right">
        <div class="profile_button">
          <div class="btn-group">
            <button type="button" class="btn btn-danger"> <i class="fa fa-user-md" aria-hidden="true"></i>
            <?= $doctor_data['d_firstname'] ?>
            </button>
            <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="sr-only"><i class="fa fa-bars" aria-hidden="true"></i></span> </button>
            <div class="dropdown-menu"> <a class="dropdown-item" href="<?php echo base_url()?>Account"> Dashboard </a> <a class="dropdown-item" href="<?php echo base_url()?>Account/edit_profile"> Profile </a> <a class="dropdown-item" href="<?php echo base_url()?>Account/appoint"> Appointments </a> <a class="dropdown-item" href="<?php echo base_url() ?>Time_slot"> Addtimeslot </a> <a class="dropdown-item" href="<?php echo base_url()?>Account/password"> Change Password </a> <a class="dropdown-item" href="<?php echo base_url()?>Account/logout"> Sign Out </a> </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
    <?php } ?>
  </div>
</div>
<!-- *************** Header Close *************** --> 

<!-- *************** Main Part Start *************** -->
<div class="singin_bg">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <label class="profile_lable">Appointments</label>
      </div>
    </div>
 
    <div class="row">
        <?php if(!empty($text)){?>
  <?php foreach($text as $text):?>
      <div class="col-md-3">
        <div class="dashboard-text">
          <div class="book_docter_details">
            <div class="book_docter_details_lable">Appointment<button class="fa fa-close btn btn-primary" data-toggle="modal" data-target="#myModal"  style=" float:right; cursor:pointer; height:26px; padding:0px 5px;"></button></div>
            <div class="doctor_details">
              <div class="book_docter_details_lable2 book_docter_details_border"> Name <br>
                <span><?= $text['p_firstname'] ?> <?= $text['p_lastname'] ?></span> </div>
              <div class="book_docter_details_lable2 book_docter_details_border"> Email <br>
                <span><?= $text['p_email'] ?></span> </div>
              <div class="book_docter_details_lable2 book_docter_details_border"> Call <br>
                <span><?= $text['p_phone'] ?></span> </div>
              <div class="book_docter_details_lable2 book_docter_details_border"> Address <br>
                <span><?= $text['s_address'] ?> <?= $text['a_address'] ?></span> </div>
              <div class="book_docter_details_lable2"> City <br>
                <span><?= $text['p_city'] ?></span> </div>
            </div>
          </div>
        </div>
      </div>
        <?php endforeach; ?>
       <?php }else { ?>
        <div class="col-md-6">
        	<h2>
            No Appointment Yet!
          </h2>
          </div>
       <?php } ?>
    </div>
      
  </div>
</div>
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myModalLabel">Cancel Your Appointment</h4>
                                            </div>
                                            <div class="modal-body">
                                                <h4>Are You Sure ??</h4>
                                                <p>you want to chancel ur appointment</p>
                                               
                                                
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  <a href="<?php echo base_url();?>Chancel_appointment/chancel_appoint?app_id=<?= $app_data['app_id'] ?>&slot_id=<?= $app_data['slot_id'] ?>" class="btn btn-primary"role="button">Chancel</a>
                                                
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div>

<!-- *************** Main Part Close *************** -->
<?php include_once ('footer.php'); ?>

</body>
</html>
