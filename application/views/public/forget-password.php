 <?php include_once('header.php');?>
<!-- *************** Main Part Start *************** -->
    <div class="singin_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="col-md-4">
<?php echo form_open('password/change_password',['name'=>'c_password']) ?>
            <label class="singin_lable">Reset Password</label>
          </div>
          <div class="col-md-4"></div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="col-md-4">
            <div class="">
          <?php echo form_password(['name'=>'p_password','class'=>'sign_textbox','placeholder'=>'New Password'])?>  
               <?php echo form_error('p_password');?>  
           <?php echo form_password(['name'=>'cp_password','class'=>'sign_textbox','placeholder'=>'Confirm Password'])?> 
               <?php echo form_error('cp_password');?>
           <?php echo form_submit(['name'=>'submit','class'=>'sign_button','value'=>'Submit'])?>
              
            </div>
          </div>

          <div class="col-md-4"></div>
        </div>
      </div>
    </div>
 <?php echo form_close(); ?>
    <!-- *************** Main Part Close *************** -->

<?php include_once('footer.php');?>
 </body>
</html>