<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Veterilib</title>
    <?= link_tag('assets/css/bootstrap.min.css')?>
    <?= link_tag('assets/css/font-awesome.min.css')?>
    <?= link_tag('assets/css/style.css') ?>
    <?= link_tag('assets/css/bootstrap-select.min.css') ?>
    <script src="/vertilib/assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="/vertilib/assets/js/bootstrap-select.min.js" charset="utf-8"></script>
   
  </head>
  <body>
    <!-- *************** Header Start *************** -->
    <div class="header_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo_text">
              <a href="<?php echo base_url() ?>" title="Veterilib">Veterilib</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- *************** Header Close *************** -->

    <!-- Book Appointment Steps Start -->
    <div class="bookappointment_steps_bg">
      <div class="bookappointment_step1">
        <div class="bookappointment_steps_one flow-next active"> <div class="full">Patient Info</div> </div>
      </div>
      <div class="bookappointment_step2">
        <div class="bookappointment_steps_one flow-next active"> <div class="full">Appt Info</div> </div>
      </div>
      <div class="bookappointment_step3">
        <div class="bookappointment_steps_one flow-next not-active"> <div class="full">Verify</div> </div>
      </div>
      <div class="bookappointment_step4">
        <div class="bookappointment_steps_one flow-next not-active"> <div class="full">Finished!</div> </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <!-- Book Appointment Steps Start -->

    <!-- *************** Main Part Start *************** -->
    <div class="bookappointment_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="bookappointment_heading">Book Your Appointment</div>
          </div>
        </div>
       
<?php echo form_open('Booking/book_2',['name'=>'singin']) ?>
        <div class="row">
          <div class="col-md-8">
              <div class="bookappointment_div">
                <div class="bookappointment_subheading">Will you use insurance?</div>
                <div class="">
                    <input type="hidden" name ="patient_id" value="<?php echo $patient_id;?>">
                           
                    
                  <select class="selectpicker mygroup_selectbox" name="payment" required>
                    <option value="">--Select--</option>
                    <option value="I'm paying for myself">I'm paying for myself</option>
                    <optgroup label="Popular Insurances">
                      <option value="Aetna">Aetna</option>
                      <option value="Blue Cross Blue Shield">Blue Cross Blue Shield</option>
                      <option value="Cigna">Cigna</option>
                    </optgroup>
                    <optgroup label="All Insurances">
                      <option value="1199SEIU">1199SEIU</option>
                      <option value="20/20 Eyecare Plan">20/20 Eyecare Plan</option>
                      <option value="AARP">AARP</option>
                    </optgroup>
                  </select>
                </div>

                <br><br>

                <div class="bookappointment_subheading">What's the reason for your visit?</div>
                <div class="">
                  <select class="mygroup_selectbox mygroup_selectbox2" name="reason" required>
                     <option value="">--Select--</option>
                      <option value="1199SEIU">1199SEIU</option>
                      <option value="20/20 Eyecare Plan">20/20 Eyecare Plan</option>
                      <option value="AARP">AARP</option>
                  </select>
                </div>

                <br><br>

                <div class="bookappointment_subheading">Have you visited this doctor before?</div>
                <div class="bookappointment_type">
                  <div class="">
                    <label> <input type="radio" name="new_old" id="register_user" onclick="check_type(1)" value="I'm a new patient" required> &nbsp; I'm a new patient. </label>
                  </div>
                  <div class="">
                      <label> <input type="radio" name="new_old" id="login_user" onclick="check_type(2)" value="I've seen this doctor before"> &nbsp; I've seen this doctor before. </label>
                  </div>
                </div>

                <br><br>
                <?php if($booking_data != null){foreach($booking_data as $value){ ?>
                <div class="bookappointment_time">
                  <div class="bookappointment_subheading">Appointment Time</div>
                  <a><?= $value['date_from'] ?> - <?= $value['time_from'] ?></a>
                </div>
                <input type="hidden" name ="slot_id" value="<?= $value['slot_id'] ?>">
                <?php }} ?>
                <br><br>
<?php echo form_submit(['name'=>'submit','class'=>'bookappointment_login_button','id'=>'submit','value'=>'Continue'])?>

                <!--<input type="button" name="name" class="bookappointment_login_button" value="Continue">-->
                <br><br><br><br>
              </div>
          </div>
            <?php echo form_close(); ?>
<?php if($booking_data != null){foreach($booking_data as $value){ ?>
          <div class="col-md-4">
            <div class="book_docter_details">
              <div class="book_docter_details_lable">Doctor</div>

              <div class="doctor_details">
                <div class="">
                  <div class="col-md-4">
                    <div class="book_doctor_img">
                      <img src=<?= $value['d_image'] ?> class="img-circle" alt="" width="80"/>
                    </div>
                  </div>
                  <div class="col-md-8">
                    <label class="book_doctor_name">Dr. <?= $value['d_firstname'] ?> <?= $value['d_lastname'] ?></label>
                    <div class="book_doctor_reating">
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star-o" aria-hidden="true"></i>
                    </div>
                    <div class="book_doctor_text">
                      <p><?= $value['d_city'] ?> - <?= $value['zipcode'] ?></p>
                    </div>
                  </div>
                  <div class="clear"></div>
                </div>

                <div class="book_docter_details_lable2 border_book_docter_details"><?= $value['date_from'] ?>-<?= $value['time_from'] ?></div>

                <div class="book_docter_details_lable2">
                  specialty <br>
                  <span><?= $value['d_specialty'] ?></span>
                </div>

                <div class="book_docter_details_lable2">
                  Clinic Name <br>
                  <span><?= $value['d_clinic'] ?></span>
                </div>

              </div>
            </div>

            <span class="secure_booking"> <i class="fa fa-lock" aria-hidden="true"></i> Secure Booking </span>
          </div>
                <?php }} ?>

        </div>
      </div>
    </div>
    <!-- *************** Main Part Close *************** -->
        <?php include_once('footer.php');?>

    
  </body>
</html>
