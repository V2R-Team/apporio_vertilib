<?php include_once('header.php');?>
<script src="/vertilib/assets/js/jquery.min.js" charset="utf-8"></script>
<script src="/vertilib/assets/js/bootstrap.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

<!-- *************** Main Part Start *************** -->
<?php echo form_open('Signup/new_user',['name'=>'login']) ?>
<div class="singin_bg">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
     <?php echo "<div class='error'>".form_error('p_email')."</div>";?>
        <label class="singin_lable">Create an Account.</label>
        <a href="ignin" class="singin_link">Already have one? Sign in.</a> </div>
        
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="singup_lable">Enter your email</div>
        <?php echo form_input(['name'=>'p_email','class'=>'sign_textbox','placeholder'=>'Email Adddress','value'=>set_value('p_email'),"required"=>"required"])?> </div>
      <div class="col-md-6">
        <div class="singup_lable">Confirm your email</div>
        <?php echo form_input(['name'=>'cp_email','class'=>'sign_textbox','placeholder'=>'Confirm your email',"required"=>"required"])?> <?php echo "<div class='error'>".form_error('cp_email')."</div>";?> </div>
      <div class="col-md-6">
        <div class="singup_lable">Create a password</div>
        <?php echo form_password(['name'=>'p_password','class'=>'sign_textbox','placeholder'=>'Enter Password',"required"=>"required"])?> <?php echo "<div class='error'>".form_error('p_password')."</div>";?> </div>
      <div class="col-md-6">
        <div class="singup_lable">Your name</div>
        <div class="col-md-6 padding_none"> <?php echo form_input(['name'=>'p_firstname','class'=>'sign_textbox','placeholder'=>'First Name','value'=>set_value('p_firstname'),"required"=>"required"])?> </div>
        <?php echo "<div class='error'>".form_error('p_firstname')."</div>";?>
        <div class="col-md-6 padding_none"> <?php echo form_input(['name'=>'p_lastname','class'=>'sign_textbox','placeholder'=>'Last Name','value'=>set_value('p_lastname'),"required"=>"required"])?> </div>
      </div>
      <div class="col-md-6">
        <div class="singup_lable">Date of birth</div>
        <div class="col-md-4 padding_none"> <?php echo form_input(['name'=>'p_dob','class'=>'sign_textbox','placeholder'=>'MM',"required"=>"required"])?> <?php echo "<div class='error'>".form_error('p_dob')."</div>";?> </div>
        <div class="col-md-4 padding_none"> <?php echo form_input(['name'=>'p_dob1','class'=>'sign_textbox','placeholder'=>'DD',"required"=>"required"])?> </div>
        <div class="col-md-4 padding_none"> <?php echo form_input(['name'=>'p_dob2','class'=>'sign_textbox','placeholder'=>'YYYY',"required"=>"required"])?> </div>
      </div>
      <div class="col-md-6">
        <div class="singup_lable"></div>
        <div class="funkyradio padding_none">
          <div class="funkyradio-primary col-md-6 padding_none">
            <input type="radio" name="radio" id="radio1" value="male" checked/>
            <label for="radio1">Male</label>
          </div>
          <div class="funkyradio-primary col-md-6 padding_none">
            <input type="radio" name="radio" id="radio2" value="female"/>
            <label for="radio2">Female</label>
          </div>
        </div>
      </div>
      <div class="clear"></div>
      <div class="col-md-6">
        <div class="terms_lable_signup">
          <label> <span class="terms_lable_checkbox">
            <input type="checkbox" name="terms" id="terms" value="">
            </span> Please read and accept the <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>. </label>
        </div>
      </div>
      <div class="clear"></div>
      <div class="col-md-6" style="margin-top:25px;"> <?php echo form_submit(['name'=>'submit','class'=>'sign_button','value'=>'Save and continue'])?> </div>
    </div>
    <br>
  </div>
  <div class="col-md-3"></div>
</div>
</div>
</div>
<?php echo form_close(); ?> 
<!-- *************** Main Part Close *************** -->

<?php include_once('footer.php');?>
</body></html>