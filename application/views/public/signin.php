<!DOCTYPE html>
 <?php include_once('header.php');?>

    <!-- *************** Main Part Start *************** -->
    <div class="singin_bg">
      <div class="container">
        <div class="row">
          <div class="col-md-1"></div>
          <div class="col-md-4">
            <label class="singin_lable">Sign in or create an account</label>
          </div>
          <div class="col-md-7"></div>
        </div>

        <div class="row">
          <div class="col-md-1"></div>
          <?php echo form_open('Signin/login_user',['name'=>'login']) ?>
          <div class="col-md-4">
            <div class="">
              <div class="singin_lable2">Sign into your vertilib account</div>
             <?php  if (isset($data)){
    echo "<div class='error' style=color:red;>$error</div>";
}?>
              <?php echo form_input(['name'=>'p_email','class'=>'sign_textbox','placeholder'=>'Email Adddress','value'=>set_value('p_email')])?>
              <?php echo "<div class='error'>".form_error('p_email')."</div>";?>
              <?php echo form_password(['name'=>'p_password','class'=>'sign_textbox','placeholder'=>'Enter Password'])?>
              <?php echo "<div class='error'>".form_error('p_password')."</div>";?>
              <?php echo form_submit(['name'=>'submit','class'=>'sign_button','value'=>'Sign in'])?>
               <?php echo form_close(); ?>
             
               <?= anchor('password','Forgot your password?',['class'=>'singin_forgot_link'])?>
            </div>
          </div>

          <div class="col-md-2">
            <div class="singin_part_border"></div>
          </div>

          <div class="col-md-4">
            <div class="">
              <div class="singin_lable2">New to Vertilib?</div>
              <?php echo anchor('Signup', 'Create an account', 'class="sign_button2"') ?>
              
              
            </div>
          </div>
          <div class="col-md-1"></div>
        </div>
      </div>
    </div>
    <!-- *************** Main Part Close *************** -->

    <?php include_once('footer.php');?>

   <script src="assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="assets/js/bootstrap.min.js" charset="utf-8"></script>
  </body>
</html>