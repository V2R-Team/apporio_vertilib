<?php

class Usermodel extends CI_Model{

     public function login_user($email,$password){


         $query = $this->db->where(['p_email'=>$email,'p_password'=>$password])
                           ->get('patient');

         if($query->num_rows()){
             return $query->row()->patien_id;
         }
         else{
             return FALSE;
         }
     }
     public function new_user1($data){


         if( $this->db->set($data)
                    ->insert('patient'))
         {
            return true ;
         }
         else
         {
             return false;
         }
        
     }
         public function doc_user($email,$password){


         $query = $this->db->where(['d_email'=>$email,'d_password'=>$password])
                           ->get('doctor');

         if($query->num_rows()){
             return $query->row()->doctor_id;
         }
         else{
             return FALSE;
         }
     }
         public function user_signup($data){
      $q = $this->db->set($data)
              ->insert('patient');
        return $this->db->insert_id();    
     } 
     
    public function user_data($data){
      $q = $this->db->where(['patient_id'=>$data])
                    ->get('patient');
        return $q->result_array();    
     } 
    public function doc_data($data){
      $q = $this->db->where(['doctor_id'=>$data])
                    ->get('doctor');
        return $q->result_array();    
     } 
      function get_autocomplete($city){ 
        $this->db->select('*')->from('city'); 
        $this->db->like('d_city',$city); 
        
        $query = $this->db->get();     
        return $query->result(); 
    } 
      function get_clinic($key){ 
        $this->db->select('*')->from('doctor'); 
        $this->db->like('d_firstname',$key); 
        
        $query = $this->db->get();     
        return $query->result(); 
    } 
}

