<?php

class Passwordmodel extends CI_Model{
    public function get_password(){
        $session_data = $this->session->userdata('patient_id');
        $query = $this->db->where(['patient_id'=>$session_data])
                           ->get('patient');
        if($query->num_rows()){
             return $query->row();
         }
         else{
             return FALSE;
         }
        
         }
         public function update_password($p_id,$old_password,$new_password){
           $d = array(
               'p_password' => $new_password
                   );
            $this->db->where('patient_id', $p_id)
           	->where('p_password', $old_password)
                    ->update('patient', $d);
         }
 public function change_password($doctor_id,$old_password,$new_password){
           $d = array(
               'd_password' => $new_password
                   );
            $this->db->where('doctor_id',$doctor_id)
           	->where('d_password', $old_password)
                    ->update('doctor', $d);
         }
    
}

