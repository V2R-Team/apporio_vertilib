<?php
class Data extends CI_Model{
    public function city(){
        $query = $this->db
                          ->distinct('zipcode')
                          ->select('*')
                          ->from('city')
                           ->get();
         return $query->result_array();

    }
    public function specialty(){
        $q = $this->db
            ->distinct('d_specialty')
            ->select('*')
            ->from('specialty')
            ->get();
          return $q->result_array();

    }
    public function animal(){
        $q = $this->db
            ->distinct('animal_name')
            ->select('*')
            ->from('animal')
            ->get();
          return $q->result_array();

    }
    public function appointment($doctor_id){
       
      
        $q = $this->db
                ->from('appointment')
                ->join('patient', 'patient.patient_id = appointment.patient_id','inner')
                ->where(array('appointment.doctor_id'=> $doctor_id))
                ->get();
          return $q->result_array();
    
}
 public function appointment_user($data){
      
     
        $q = $this->db
                ->from('appointment')
                ->join('doctor', 'doctor.doctor_id = appointment.doctor_id','inner')
                ->join('city', 'city.city_id = doctor.city_id','inner')
                ->join('specialty', 'specialty.specialty_id = doctor.specialty_id','inner')
                ->where(array('appointment.patient_id'=> $data))
                ->get();
          return $q->result_array();
         
    
}
}