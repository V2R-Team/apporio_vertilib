<?php
class Adminmodel extends CI_Model{

     public function login_user($email,$password){


         $query = $this->db->where(['p_email'=>$email,'p_password'=>$password])
                           ->get('patient');

         if($query->num_rows()){
             return $query->row()->patient_id;
         }
         else{
             return FALSE;
         }
     }
     
     public function user_signup($data){
      $q = $this->db->set($data)
              ->insert('doctor');
        return $this->db->insert_id();    
     }
     
     public function slot_detail($slot_id) {
          $query = $this->db->where(['slot_id'=>$slot_id])
                           ->get('time_slot');
         
     
     if($query->num_rows()){
             return $query->row_array();
         }
         else{
             return FALSE;
         }
     }
     public function booking_finish($data,$patient_id,$payment,$reason,$new_old,$slot_id){
       
        $d = array( 'status' => 0);
       $this->db->where('slot_id', $data['slot_id']);
       $this->db->update('time_slot', $d);   

        
         $this->db->set(['patient_id'=>$patient_id,'doctor_id'=>$data['doctor_id'],'app_date'=>$data['date_from'],'app_time'=>$data['time_from'],'payment'=>$payment,'reason'=>$reason,'new_old'=>$new_old,'slot_id'=>$slot_id,'booking_d_t'=>date('Y-m-d H:i:s',time())])
              ->insert('appointment');
}
 
        

}