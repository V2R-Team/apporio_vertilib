<?php 
class Time_slotmodel extends CI_Model{

     public function get_time($doctor){

      $q = $this->db->select('*')
                    ->where(['doctor_id'=> $doctor])
                    ->order_by("slot_id", "desc")
                    ->get('time_slot');
          
          if($q->num_rows()){
             return $q->result_array();
         }
         else{
             return FALSE;
         }
       
     }
     public function add_time($date,$time,$doctor){


      $q = $this->db->set(['doctor_id'=>$doctor,'date_from'=>$date,'time_from'=>$time])
                    ->insert('time_slot');
       
     }
     public function remove_time($date,$time,$doctor)
     {
	     $this->db->where('doctor_id', $doctor);
	     $this->db->where('date_from', $date);
	     $this->db->where('time_from', $time);
	      $this->db->delete('time_slot');
     
     }
}
