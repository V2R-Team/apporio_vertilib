<?php
class SearchModel extends CI_Model{
    public function specality($search){
   $this->db->select('d_specialty');
     $whereCondition = array('d_specialty' =>$search);
     $this->db->where($whereCondition);
     $this->db->from('specialty');
       $query = $this->db->get();
     return $query->result();
    }

    public function time_slot($specialty,$date_slot,$search_from){
      
       switch($search_from)
       {
       
       	case 'symptoms':
       			$query =  $this->db->select('*')
                             ->from('time_slot') 
                             ->join('doctor', 'doctor.doctor_id = time_slot.doctor_id','inner')
                             ->join('specialty', 'specialty.specialty_id = doctor.specialty_id','inner')
                             
                             ->where("specialty.symptom_list LIKE '%$specialty%'")
                             ->where(array('time_slot.date_from' => $date_slot,'time_slot.status' => 1))
                             ->get();
 
                       return $query->result_array();
                       break;
        case 'clinic':
       			$query =  $this->db->select('*')
                             ->from('time_slot') 
                             ->join('doctor', 'doctor.doctor_id = time_slot.doctor_id','inner')
                             ->join('specialty', 'specialty.specialty_id = doctor.specialty_id','inner')
                             ->where("doctor.d_clinic = '$specialty'")
                             ->where(array('time_slot.date_from' => $date_slot,'time_slot.status' => 1))
                             ->get();
 
                       return $query->result_array();
                       break;               
        case 'animal':
       			$query =  $this->db->select('*')
                             ->from('time_slot') 
                             ->join('doctor', 'doctor.doctor_id = time_slot.doctor_id','inner')
                             ->join('specialty', 'specialty.specialty_id = doctor.specialty_id','inner')
                             
                             ->where("doctor.animal LIKE '%$specialty%'")
                             ->where(array('time_slot.date_from' => $date_slot,'time_slot.status' => 1))
                             ->get();
                                                    return $query->result_array();
                       break;
         case 'doctor':
       			$query =  $this->db->select('*')
                             ->from('time_slot') 
                             ->join('doctor', 'doctor.doctor_id = time_slot.doctor_id','inner')
                             ->join('specialty', 'specialty.specialty_id = doctor.specialty_id','inner')
                             ->where("doctor.d_firstname = '$specialty'")
                             ->where(array('time_slot.date_from' => $date_slot,'time_slot.status' => 1))
                             ->get();
 
                       return $query->result_array();
                       break; 
         default:
                    $query =  $this->db->select('*')
                             ->from('time_slot') 
                             ->join('doctor', 'doctor.doctor_id = time_slot.doctor_id','inner')
                             ->join('specialty', 'specialty.specialty_id = doctor.specialty_id','inner')
                             ->where("specialty.d_specialty LIKE '%$specialty%'")
                             ->where(array('time_slot.date_from' => $date_slot,'time_slot.status' => 1))
                             ->get();

                       return $query->result_array();
                       break;                  
       }
          
    }
    public function booking_slot($slot_id){
      
       
          $query =  $this->db->select('*')
                             ->from('time_slot') 
                             ->join('doctor', 'doctor.doctor_id = time_slot.doctor_id','inner')
                             ->join('city', 'city.city_id = doctor.city_id','inner')
                             ->join('specialty', 'specialty.specialty_id = doctor.specialty_id','inner')
                             ->where(array('time_slot.slot_id' => $slot_id,'time_slot.status' => 1))
                             ->get();

                     return $query->result_array();
    }
    
     public function get_patient($patient_id){
      
       
          $query =  $this->db->select('p_firstname,p_email')
                             ->from('patient') 
                             
                             ->where(array('patient_id' => $patient_id))
                             ->get();

                     return $query->result_array();
    }
    
    public function city_slot($city,$date_slot){
    $c = explode('-',$city,2);
     
     
     
        
         $query =  $this->db->select('*')
                             ->from('time_slot') 
                             ->join('doctor', 'doctor.doctor_id = time_slot.doctor_id','inner')
                             ->join('city', 'city.city_id = doctor.city_id','inner')
                             ->join('specialty', 'specialty.specialty_id = doctor.specialty_id','inner')
                             ->where(array('time_slot.date_from' => $date_slot,'time_slot.status' => 1))
                             ->like('city.d_city', $c[0])
                             ->get();

                     return $query->result_array();
    }
     public function doctor_slot($city){
    $c = explode('-',$city,2);
         $query =  $this->db->select('*')
                             ->from('doctor') 
                             
                             ->join('city', 'city.city_id = doctor.city_id','inner')

                             ->like('city.d_city', $c[0])
                             ->get();

                     return $query->result_array();
    }
    public function doctor_name($specialty){
    $query =  $this->db->select('*')
                             ->from('doctor') 
                              ->join('city', 'city.city_id = doctor.city_id','inner')
                             ->like('d_firstname' , $specialty)
                             ->get();
 
                       return $query->result_array();
                       }
           public function doctor_data($specialty,$city){
           $c = explode('-',$city,2);
    $query =  $this->db->select('*')
                             ->from('doctor') 
                             ->join('city', 'city.city_id = doctor.city_id','inner')
                             ->like('d_firstname' , $specialty)
                             ->like('city.d_city', $c[0])
                             ->get();
 
                       return $query->result_array();
                       }
    public function cs_slot($specialty,$city,$date_slot){
          $c = explode('-',$city,2);
          $query =  $this->db->select('*')
                             ->from('time_slot') 
                             ->join('doctor', 'doctor.doctor_id = time_slot.doctor_id','inner')
                             ->join('city', 'city.city_id = doctor.city_id','inner')
                             ->join('specialty', 'specialty.specialty_id = doctor.specialty_id','inner')
                             ->where(array('specialty.d_specialty' => $specialty,'time_slot.date_from' => $date_slot,'time_slot.status' => 1))
                             ->like('city.d_city', $c[0])
                             ->get();
                     return $query->result_array();
    }
    
    
}