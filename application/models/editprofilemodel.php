<?php

class Editprofilemodel extends CI_Model{
    public function get_id($doctor_id){ 
 
         $query = $this->db->select('*')
                             ->from('doctor') 
                             ->join('city', 'city.city_id = doctor.city_id','inner')
                             ->join('specialty', 'specialty.specialty_id = doctor.specialty_id','inner')
                        ->where(['doctor_id'=>$doctor_id])
                           ->get();
          if($query->num_rows()){
             return $query->row_array();
         }
         else{
             return FALSE;
         }
    }
    public function edit_profile($data){
      $doctor = $this->session->userdata('doctor_id');
         $this->db->where('doctor_id', $doctor);
          $this->db->update('doctor', $data); 
         $query = $this->db->select('*')
                        ->where(['doctor_id'=>$doctor])
                           ->get('doctor');
          if($query->num_rows()){
             return $query->row_array();
         }
         else{
             return FALSE;
         }    
                           
    }
        public function edit_user($data){
      $user = $this->session->userdata('patient_id');
         $this->db->where('patient_id', $user);
          $this->db->update('patient', $data); 
         $query = $this->db->select('*')
                        ->where(['patient_id'=>$user])
                           ->get('patient');
          if($query->num_rows()){
             return $query->row_array();
         }
         else{
             return FALSE;
         }    
                           
    }
}

