-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:24 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_vertilib_ci`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_username`, `admin_password`, `admin_email`, `admin_status`) VALUES
(1, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `app_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `app_date` varchar(200) NOT NULL,
  `app_time` varchar(50) NOT NULL,
  `booking_d_t` varchar(600) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`app_id`, `patient_id`, `doctor_id`, `app_date`, `app_time`, `booking_d_t`) VALUES
(4, 16, 2, '2017-01-02', '01:02', '2017-01-01 05:00:00'),
(5, 17, 12, '2017-01-02', '01:02', '2017-01-01 05:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `d_city` varchar(200) NOT NULL,
  `zipcode` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `d_city`, `zipcode`) VALUES
(4, 'Mazamet \r\n', '81200 '),
(5, 'Lavaur\r\n', ' 81500 '),
(6, 'Castres \r\n', '81100'),
(7, 'Gaillac \r\n', '81600'),
(8, 'Castres\r\n', '81100'),
(9, 'Castres\r\n', '81100'),
(10, 'Avenue Terres Noires \r\n', '1163 '),
(11, 'Labruguière \r\n', '81200'),
(12, 'Saint-Juéry \r\n', '81160 '),
(13, 'Aussillon\r\n', '81200 '),
(14, 'Castres ', '81100\r\n'),
(15, 'Gaillac \r\n', '81600 ');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `doctor_id` int(11) NOT NULL,
  `username` varchar(80) NOT NULL,
  `d_password` varchar(20) NOT NULL,
  `d_firstname` varchar(40) NOT NULL,
  `d_lastname` varchar(40) NOT NULL,
  `specialty_id` int(11) NOT NULL,
  `d_email` varchar(50) NOT NULL,
  `d_dob` varchar(50) NOT NULL,
  `d_phone` varchar(50) NOT NULL,
  `d_workphone` bigint(20) NOT NULL,
  `d_homephone` bigint(20) NOT NULL,
  `prefer_no` varchar(60) NOT NULL,
  `city_id` int(11) NOT NULL,
  `d_address` varchar(2000) NOT NULL,
  `d_gender` varchar(50) NOT NULL,
  `d_image` varchar(900) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`doctor_id`, `username`, `d_password`, `d_firstname`, `d_lastname`, `specialty_id`, `d_email`, `d_dob`, `d_phone`, `d_workphone`, `d_homephone`, `prefer_no`, `city_id`, `d_address`, `d_gender`, `d_image`) VALUES
(2, 'malik', '123456789', 'malik', 'barar', 1, 'cs@gmail.com', '', '98765465276', 0, 0, '', 9, '', 'male', ''),
(9, '', '12345678', 'gream', 'smith', 2, 'smith@gmail.com', '', '9999999999', 0, 0, '', 4, '', '', ''),
(10, '', '12345678', 'gream', 'smith', 1, 'smith@gmail.com', '', '9999999999', 0, 0, '', 10, '', '', ''),
(11, '', '12345678', 'Clinique du Parc', 'Clinique du Parc', 4, 'clinique@gmail.com', '', '9898989898', 0, 0, '', 4, '', '', ''),
(12, '', '1234567891', 'Clinique Vétérinaire des Docteurs', 'Docteurs', 5, 'docteurs@gmail.com', '', '9999999999', 0, 0, '', 7, '', '', ''),
(13, '', 'sabrinasabrina', 'Faycal', 'Ihr', 3, 'pro.fihrai@gmail.com', '', '060606060606', 0, 0, '', 11, '', '', ''),
(14, '', 'password123', 'Harry', 'Wise', 1, 'harry14@gmail.com', '', '9856345566', 0, 0, '', 4, '', '', ''),
(15, '', 'password', 'Philip', 'paul', 1, 'philp@gmail.com', '', '987563452', 0, 0, '', 8, '', '', ''),
(16, '', '111111', 'deborah', 'kolade', 2, 'garg26.shilpa@gmail.com', '', '08130039030', 0, 0, '', 6, '', '', ''),
(17, '', '1234', 'sumit', 'singh', 1, 'sumit@gmail.com', '', '09782833224', 0, 0, '', 4, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `master_pages`
--

CREATE TABLE `master_pages` (
  `page_id` int(11) NOT NULL,
  `page_name` varchar(255) NOT NULL,
  `page_slug` varchar(255) NOT NULL,
  `page_excerpt` longtext NOT NULL,
  `page_description` longtext NOT NULL,
  `page_img` varchar(255) NOT NULL,
  `page_status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_pages`
--

INSERT INTO `master_pages` (`page_id`, `page_name`, `page_slug`, `page_excerpt`, `page_description`, `page_img`, `page_status`) VALUES
(1, 'About Us', 'about-us', 'Crée en 2016 à Paris, Veterilib est la première plateforme de mise en relation entre propriétaires d\'animaux et les vétérinaires.<p>Veterilib permet à tous les propriétaires d\'animaux de chercher des vétérinaires disponibles près de chez eux, de lire les avis certifiés et de réserver en quelques clics un rendez-vous.</p><p>Le service est gratuit, disponible 24/24 7/7 et entièrement sécurisé.&nbsp;</p>', '', 'images/pages/about-us.jpg', 1),
(2, 'Contact Us', 'contact-us', 'Vous pouvez nous joindre par e-mail veterilib@gmail.com', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'images/pages/contact-us.jpg', 1),
(3, 'Press', 'press', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'images/pages/press.png', 1),
(4, 'Careers', 'careers', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'images/pages/careers.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `patient_id` int(11) NOT NULL,
  `p_firstname` varchar(50) NOT NULL,
  `p_lastname` varchar(50) NOT NULL,
  `p_email` varchar(50) NOT NULL,
  `p_password` varchar(20) NOT NULL,
  `p_dob` varchar(300) NOT NULL,
  `p_sex` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`patient_id`, `p_firstname`, `p_lastname`, `p_email`, `p_password`, `p_dob`, `p_sex`) VALUES
(16, 'aamir', 'brar', 'er.mohdaamir@gmail.com', 'nvv', '1993-31-08', 'male'),
(17, 'Shilpa', 'Goyal', 'garg26.shilpa@gmail.com', 'password123', '1988-26-12', 'female'),
(18, 'j', 'j', 'test@test.com', '0000000000', '2000-01-20', 'male'),
(19, 'ankit', 'kumar', 'ankit@gmail.com', '1234567891', '1992-12-12', 'male'),
(20, 'Faycal', 'Ihr', 'pro.fihrai@gmail.com', 'sabrinasabrina', '1980-01-02', 'male');

-- --------------------------------------------------------

--
-- Table structure for table `specialty`
--

CREATE TABLE `specialty` (
  `specialty_id` int(11) NOT NULL,
  `d_specialty` varchar(600) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `specialty`
--

INSERT INTO `specialty` (`specialty_id`, `d_specialty`) VALUES
(1, 'Animal behavior'),
(2, 'Anatomie pathologique vétérinaire\r\n'),
(3, 'Chirurgie des animaux de compagnie\r\n'),
(4, 'Chirurgie équine\r\n'),
(5, 'Dermatologie vétérinaire\r\n'),
(6, 'Elevage et pathologie des équidés - option biomécanique et pathologie de l\'appareil locomoteur\r\n'),
(7, 'Elevage et pathologie des équidés - option chirurgie équine\r\n'),
(8, 'Gestion de la santé des bovins\r\n'),
(9, 'Gestion de la santé porcine\r\n'),
(10, 'Imagerie médicale vétérinaire'),
(11, 'Médecine du Comportement des animaux de compagnie\r\n'),
(12, 'Médecine interne des animaux de compagnie\r\n'),
(13, 'Médecine interne des animaux de compagnie - option cardiologie\r\n'),
(14, 'Médecine interne des équidés\r\n'),
(15, 'Neurologie vétérinaire\r\n'),
(16, 'Nutrition clinique vétérinaire\r\n'),
(17, 'Ophtalmologie vétérinaire\r\n'),
(18, 'Pathologie clinique vétérinaire\r\n'),
(19, 'Sciences et médecine des animaux de laboratoire\r\n'),
(20, 'Stomatologie et dentisterie vétérinaire\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `time_slot`
--

CREATE TABLE `time_slot` (
  `slot_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `time_from` varchar(200) NOT NULL,
  `date_from` varchar(400) NOT NULL,
  `approval` varchar(2) NOT NULL DEFAULT '0',
  `status` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `time_slot`
--

INSERT INTO `time_slot` (`slot_id`, `doctor_id`, `time_from`, `date_from`, `approval`, `status`) VALUES
(103, 1, '01:02', '01/05/2017', '0', 1),
(104, 1, '19:11', '01/04/2017', '0', 1),
(105, 1, '08:26', '01/05/2017', '0', 1),
(113, 1, '22:08', '01/05/2017', '0', 1),
(114, 1, '12:08', '01/06/2017', '0', 1),
(115, 1, '22:09', '01/06/2017', '0', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`app_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`doctor_id`);

--
-- Indexes for table `master_pages`
--
ALTER TABLE `master_pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`patient_id`);

--
-- Indexes for table `specialty`
--
ALTER TABLE `specialty`
  ADD PRIMARY KEY (`specialty_id`);

--
-- Indexes for table `time_slot`
--
ALTER TABLE `time_slot`
  ADD PRIMARY KEY (`slot_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `app_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `doctor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `master_pages`
--
ALTER TABLE `master_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `patient_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `specialty`
--
ALTER TABLE `specialty`
  MODIFY `specialty_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `time_slot`
--
ALTER TABLE `time_slot`
  MODIFY `slot_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
